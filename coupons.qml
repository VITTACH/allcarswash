import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    id: rootCoupons;
    // color: "#90D3FE";

    Component.onCompleted: {partnerHeader.text = "QR-купоны"}

    Connections {
        target: inerText
        onAccepted: filterList(inerText.text)
    }

    function filterList(param) {
        for(var i = 0; i < couponsModel.count; i++) {
            if(couponsModel.get(i).target0.search(param) > 0)
                couponsModel.setProperty(i, "activity", 1)
            else
                couponsModel.setProperty(i, "activity", 0)
        }
    }

    ListView {
        Component.onCompleted: {
            couponsModel.clear()
            busyIndicator.visible = true
            var arr= loader.couponsModel
            arr.forEach(function(item,i,arr) {
                    couponsModel.append({
                        image_1: "ui/coupon/timerIcon.png",
                            image_0: "ui/coupon/lockedIcon.png",
                                target0: item.target0,
                                    target1: item.target1,
                                        target2: item.target2,
                                            target3: item.target3,
                                            activity: 1
                    });
            });
            busyIndicator.visible =false
        }
        id: listView
        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        model: ListModel {
            id: couponsModel
            ListElement {
                target2: 0;
                target3: 0;
                image_0: "";
                image_1: "";
                target0: "";
                target1: "";
                activity: 1;
            }
        }
        delegate: Rectangle {
            id: myrow
            visible: activity
            width: parent.width
            height: activity == 1?
                    downRow.height +
                    text1.implicitHeight +
                    text2.implicitHeight +
                    facade.toPx(100):0

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    myrow.color="#90DDDDDD";
                }
                onReleased: {
                    myrow.color = "#FFFFFF"// (index%2 == 0? "#B6D8EE": "#C6E7FD")
                }
                onPositionChanged: {
                    myrow.color = "#FFFFFF"// (index%2 == 0? "#B6D8EE": "#C6E7FD")
                }
                onClicked: {
                    loader.curCouponInd = index;
                    loader.goTo("qrc:/coupon.qml")
                }
            }

            Item {
                anchors.fill: parent
                Column {
                    width: parent.width;
                    anchors {
                        left:parent.left
                        leftMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                    spacing: facade.toPx(10)

                    Text {
                        id: text1
                        color: "#000000"
                        font {
                            pixelSize: facade.doPx(24);
                            family:trebuchetMsNorm.name
                        }
                        text: target0
                    }
                    Text {
                        id: text2
                        color: "#000000"
                        font {
                            pixelSize: facade.doPx(24);
                            family:trebuchetMsNorm.name
                        }
                        text: target1
                    }
                    Row {
                        id: downRow
                        width: parent.width
                        spacing: facade.toPx(82)
                        height:Math.max(
                               Math.max(img0.height,text3.implicitHeight),
                               Math.max(img1.height,text4.implicitHeight))
                        Row {
                            spacing: facade.toPx(20)
                            Image {
                                id: img0
                                source: image_0
                                width: facade.toPx(sourceSize.width *1.5);
                                height:facade.toPx(sourceSize.height*1.5);
                            }
                            Text {
                                id: text3
                                color: "#000000"
                                font {
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }
                                text: target2 + " / " + target3
                            }
                        }
                        Row {
                            spacing: facade.toPx(20)
                            Image {
                                id: img1
                                source: image_1
                                width: facade.toPx(sourceSize.width *1.5);
                                height:facade.toPx(sourceSize.height*1.5);
                            }
                            Text {
                                id: text4
                                color: "#000000"
                                font {
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }
                                text: target3 - target2 > 0?
                                          "Сканировать!": "Отсканировано";
                            }
                        }
                    }
                }
                Image {
                    anchors {
                        right: parent.right
                        rightMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                    source: "ui/moreIcons/next.png";
                    width: facade.toPx(sourceSize.width * 1.2)
                    height:facade.toPx(sourceSize.height* 1.2)
                }
            }
            Rectangle {
                width: parent.width
                height: 1
                color: "lightgray"
                anchors.bottom: parent.bottom
            }
        }
    }
}
