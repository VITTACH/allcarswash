#ifndef EVENTHANDLER_H
#define EVENTHANDLER_H

#include "httpnetwork.h"

using namespace std;

class EventHandler : public QObject {
    Q_OBJECT

public:
    explicit EventHandler (QObject *parent = 0);

    bool isPin = 0;
    int currentSys = 0;

    QString PathFile = "allcarswash";

    QString domain = "http://api.qrserver.com/";
signals:

public slots:
    int currentOSys();

    void openingMap(QString,QString);
    void copyText(QString);
};

#endif// EVENTHANDLER_H
