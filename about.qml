import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import "UiStyle" as UiStyle
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4

Item {
    id: rootAbout

    property int pageHeight: 0
    property int someHeight: 0
    property int plusHeight: 0

    Component.onCompleted: {
        partnerHeader.text = "О приложении";
    }

    ListView {
        id: listView

        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        delegate: Rectangle {
            width: parent.width
            height: pageHeight + someHeight;
            Column {
                Rectangle {
                    color: "#4396E9"
                    width: parent.width
                    height: facade.toPx(350)
                    Component.onCompleted: {
                        someHeight += height
                    }
                    Image {
                        anchors.centerIn: parent
                        width: facade.toPx(sourceSize.width *2)
                        height:facade.toPx(sourceSize.height*2)
                        source:"qrc:/ui/headers/logo.png"
                    }
                }

                TabView {
                    id: tabview
                    width: parent.width
                    height: pageHeight-someHeight
                    onCurrentIndexChanged: {
                        if(index == 0)
                            listView.contentY=someHeight -
                                    partnerHeader.height +
                                    facade.toPx(40)
                    }

                    Tab {
                        title:qsTr("ОПИСАНИЕ")
                        component: Rectangle {
                            width: parent.width
                            height: tabview.currentIndex==0?
                                        pageHeight =
                                        text1.implicitHeight +
                                        text2.implicitHeight +
                                        text3.implicitHeight:0
                            Column {
                                spacing: facade.toPx(40)
                                anchors {
                                    fill: parent
                                    leftMargin: 0.02 * parent.width
                                    rightMargin:0.02 * parent.width
                                }
                                Text {
                                    id: text1
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    font {
                                        pixelSize: facade.doPx(20);
                                        family:trebuchetMsLight.name
                                    }
                                    text: "\nВсе права на приложение, тексты и графические элементы использованные в мобильном приложении 'Все Автомойки' принадлежат ООО 'Лидер' г.Санкт-Петербург, в лице Щербакова Андрея Александровича."
                                }
                                Text {
                                    id: text2
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    font {
                                        pixelSize: facade.doPx(16);
                                        family:trebuchetMsLight.name
                                    }
                                    text: "При использовании материалов из данного приложения на сторонних ресурсах, ссылка на приложение либо на сайт <a href='http://www.vse-avtomoyki.ru'>vse-avtomoyki.ru</a> обязательна.<br><br>По всем спорным вопросам, пожеланиям и предложениям обращаться по координатам указанным во вкладке 'Контакты' или<br><br>e-mail: admin@vse-avtomoyki.ru<br><br>Тел: 8-965-047-0000<br><br>web: <a href='http://www.vse-avtomoyki.ru'>vse-avtomoyki.ru</a><br><br>Разработчик - Лаборатория Мобильных Решений<br><br>web: <a href='http://www.app-labs.ru'>app-labs.ru</a><br><br>e-mail: info@app-labs.ru<br><br>Тел: +7(812)985-86-81"
                                    onLinkActivated: Qt.openUrlExternally(link)
                                    MouseArea {
                                        anchors.fill: parent
                                        acceptedButtons: Qt.NoButton // we don't want to eat clicks on the Text
                                        cursorShape: parent.hoveredLink? Qt.PointingHandCursor : Qt.ArrowCursor
                                    }
                                }
                                Text {
                                    id: text3
                                    width: parent.width
                                    horizontalAlignment: Text.AlignHCenter
                                    wrapMode: Text.Wrap
                                    font {
                                        pixelSize: facade.doPx(16);
                                        family:trebuchetMsNorm.name
                                    }
                                    text: "Санкт-Петербург 2015г."
                                }
                            }
                        }
                    }
                    Tab {
                        title: qsTr("КОММЕНТАРИИ")
                        component: Rectangle {
                            width: parent.width
                            height:tabview.currentIndex == 1?
                                        pageHeight = Math.max(
                                           plusHeight + innerRow.height,
                                           dialogSocials.pageHeight): 0;
                            Item {
                                id: innerRow
                                anchors {
                                    left: parent.left;
                                    right:parent.right
                                    leftMargin:facade.toPx(20)
                                }
                                height: Math.max(itext1.implicitHeight, ibutton1.height) + facade.toPx(40)

                                Text {
                                    id: itext1
                                    anchors {
                                        verticalCenter: parent.verticalCenter
                                    }

                                    font.bold: true
                                    font.family:trebuchetMsNorm.name
                                    font.pixelSize: facade.doPx(20);
                                    text: commetModel.count + " Комментарии";
                                }
                                Button {
                                    id: ibutton1
                                    background: Rectangle {
                                        color: "transparent"
                                    }
                                    anchors {
                                        right: parent.right
                                        rightMargin: facade.toPx(20)
                                        verticalCenter: parent.verticalCenter
                                    }
                                    onClicked: loader.social = true;

                                    font.family:trebuchetMsNorm.name
                                    font.pixelSize: facade.doPx(20);
                                    text: "ОПУБЛИКОВАТЬ"
                                }
                            }
                            ListView {
                                height:plusHeight
                                width: parent.width
                                anchors.top: innerRow.bottom
                                boundsBehavior: Flickable.StopAtBounds

                                Component.onCompleted: {
                                    commetModel.clear();
                                    for(var i = 0; i < loader.commentModel.length; i++) {
                                        commetModel.append({
                                            image: loader.commentModel[i].avatar,
                                            target1: loader.commentModel[i].comment,
                                            target0: loader.commentModel[i].fullName
                                        })
                                    }
                                }

                                model:ListModel {
                                    id: commetModel
                                    ListElement {
                                        image: ""; target0:""; target1:""
                                    }
                                }

                                delegate: Rectangle {
                                    x:facade.toPx(20)
                                    width: parent.width
                                    height: Math.max(bug.height, comment.implicitHeight) + facade.toPx(30)
                                    Component.onCompleted: {
                                        plusHeight += height
                                    }

                                    Rectangle {
                                        id: bug
                                        clip: true
                                        smooth: true
                                        visible: false
                                        width: facade.toPx(120)
                                        height:facade.toPx(120)
                                        anchors {
                                            top: parent.top
                                            topMargin: facade.toPx(20)
                                        }
                                        Image {
                                            source:image
                                            height:sourceSize.width>sourceSize.height? facade.toPx(120): sourceSize.height * (parent.width/sourceSize.width)
                                            width: sourceSize.width>sourceSize.height? sourceSize.width* (parent.height/sourceSize.height): facade.toPx(120)
                                        }
                                    }

                                    Image {
                                        id: mask
                                        smooth: true;
                                        visible:false
                                        source: "ui/uimask/roundMask.png"
                                        sourceSize: {
                                            Qt.size(facade.toPx(120), facade.toPx(120))
                                        }
                                    }

                                    OpacityMask {
                                        source: bug
                                        maskSource: mask
                                        anchors.fill: bug
                                    }

                                    Column {
                                        id: comment
                                        width: parent.width
                                        anchors {
                                            left: bug.right
                                            leftMargin: facade.toPx(40)
                                            verticalCenter: parent.verticalCenter
                                        }
                                        spacing:facade.toPx(20)
                                        Text {
                                            width: parent.width - bug.width - facade.toPx(80)
                                            wrapMode: Text.Wrap

                                            font.family:trebuchetMsNorm.name
                                            font.pixelSize: facade.doPx(20);
                                            font.bold: true
                                            text: target0
                                        }
                                        Text {
                                            width: parent.width - bug.width - facade.toPx(80)
                                            wrapMode: Text.Wrap

                                            font.family:trebuchetMsNorm.name
                                            font.pixelSize: facade.doPx(20);
                                            text: target1
                                        }
                                    }
                                }
                            }
                            UiStyle.DialogSocialm {
                                width: parent.width
                                height:dialogSocials.pageHeight
                                id: dialogSocials
                            }
                        }
                    }
                    style:TabViewStyle {
                        tab: Rectangle {
                            Component.onCompleted:someHeight+=implicitHeight
                            color: styleData.selected? "#5BB1FF": "#3383DF";
                            implicitHeight: facade.toPx(100)
                            implicitWidth: rootAbout.width/2+1;
                            Text {
                                color: "#FFFFFF"
                                width: parent.width
                                wrapMode: Text.Wrap
                                anchors.centerIn: parent
                                horizontalAlignment:{
                                    Text.AlignHCenter
                                }
                                text: styleData.title
                                font {
                                    pixelSize: facade.doPx(20);
                                    family:trebuchetMsNorm.name
                                }
                            }
                            Rectangle {
                                anchors.bottom: {parent.bottom}
                                height: facade.toPx(5)
                                width: parent.width
                                color: "#5BB1FF"
                            }
                        }
                    }
                }
                anchors.fill: parent
            }
        }
        model: ListModel {ListElement {tr:""}}
        boundsBehavior: Flickable.StopAtBounds
    }
}
