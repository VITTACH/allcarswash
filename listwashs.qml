import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Rectangle {
    id: rootWashs

    Component.onCompleted: {
        partnerHeader.text = qsTr("Список моек")
    }

    ListView {
        width: parent.width
        anchors {
            top: header.bottom
            bottom:parent.bottom
        }

        Component.onCompleted: {
            washesModel.clear();
            busyIndicator.visible = true
            var arr= loader.allWashModel
            arr.forEach(function(item,i,arr) {
                if (currentButon==item.type)
                    washesModel.append({
                        image: item.image,
                            target0: item.target0,
                                target1: item.target1,
                                    target2: item.target2,
                                        target3: item.target3
                    });
            });
            busyIndicator.visible =false
        }

        model:ListModel {
            id: washesModel
            ListElement {
                image: "";
                target0: "";
                target1: "";
                target2: "";
                target3: "";
            }
        }
        delegate: Rectangle {
            width: parent.width
            height: Math.max(bug.height, inerColumn.implicitHeight) + facade.toPx(40)

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    parent.color="#90DDDDDD"
                }
                onReleased:{
                    parent.color = "#FFFFFF"
                }
                onPositionChanged: {
                    parent.color = "#FFFFFF"
                }
                onClicked: {
                    curWashIndex=index
                    loader.goTo("qrc:/wahsinfo.qml")
                }
            }

            Item {
                anchors.fill: parent
                Item {
                    height: bug.height
                    width: parent.width
                    anchors {
                        left:parent.left
                        leftMargin: facade.toPx(40);
                        verticalCenter: parent.verticalCenter
                    }

                    Item {
                        id: item
                        width: bug.width
                        Rectangle {
                            id: bug
                            clip: true
                            smooth: true
                            visible: false
                            width: facade.toPx(200);
                            height:facade.toPx(200);
                            Image {
                                source: image
                                height:sourceSize.width > sourceSize.height? facade.toPx(200):sourceSize.height * (parent.width/sourceSize.width)
                                width: sourceSize.width > sourceSize.height? sourceSize.width*(parent.height/sourceSize.height): facade.toPx(200)
                            }
                        }

                        Image {
                            id: mask
                            smooth: true
                            visible: false
                            source: "ui/uimask/roundMask.png"
                            sourceSize: Qt.size(facade.toPx(300), facade.toPx(300))
                        }

                        OpacityMask {
                            source: bug
                            maskSource: mask
                            anchors.fill: bug
                        }
                    }

                    Column {
                        id: inerColumn
                        anchors {
                            left: item.right
                            leftMargin: facade.toPx(40)
                            verticalCenter: parent.verticalCenter
                        }
                        width: parent.width/2
                        spacing: facade.toPx(20)
                        Text {
                            text: target0
                            color: "#000000"
                            width: parent.width
                            elide: Text.ElideRight
                            font {
                                family:trebuchetMsNorm.name
                                pixelSize: facade.doPx(24);
                            }
                        }
                        Row {
                            width: parent.width
                            spacing: facade.toPx(20)
                            Image {
                                source:"ui/washs/icons/pointIcon.png"
                                height:facade.toPx(sourceSize.height*1.5)
                                width: facade.toPx(sourceSize.width *1.5)
                            }
                            Text {
                                text: target1
                                color: "#3589E9"
                                width: parent.width
                                elide: Text.ElideRight
                                font {
                                    family:trebuchetMsNorm.name
                                    pixelSize: facade.doPx(20);
                                }
                            }
                        }
                        Row {
                            width: parent.width
                            spacing: facade.toPx(20)
                            Image {
                                source: "ui/washs/icons/waysIcon.png"
                                height:facade.toPx(sourceSize.height*1.5)
                                width: facade.toPx(sourceSize.width *1.5)
                            }
                            Text {
                                text: target2 + "км. "
                                color: "#3589E9"
                                width: parent.width
                                elide: Text.ElideRight
                                font {
                                    family:trebuchetMsNorm.name
                                    pixelSize: facade.doPx(20);
                                }
                            }
                        }
                        Row {
                            width: parent.width
                            spacing: facade.toPx(20)
                            Image {
                                source:"ui/washs/icons/priceIcon.png"
                                height:facade.toPx(sourceSize.height*1.5)
                                width: facade.toPx(sourceSize.width *1.5)
                            }
                            Text {
                                text: target3
                                color: "#3589E9"
                                width: parent.width
                                elide: Text.ElideRight
                                font {
                                    family:trebuchetMsNorm.name
                                    pixelSize: facade.doPx(20);
                                }
                            }
                        }
                    }
                }
                Image {
                    source: "ui/moreIcons/next.png"
                    width: facade.toPx(sourceSize.width * 1.2)
                    height:facade.toPx(sourceSize.height* 1.2)
                    anchors {
                        right: parent.right
                        rightMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    width: parent.width
                    color: "#DDDDDD"
                    height: 1
                }
            }
        }
    }

    Rectangle {
        id: header
        width: parent.width
        height:facade.toPx(120)
        anchors {
            top: parent.top
            topMargin: partnerHeader.height - facade.toPx(10)
        }
        Rectangle {
            height: 1
            color: "#DDDDDD"
            width: parent.width
            anchors.bottom: {parent.bottom}
        }
        Text {
            id: inerText
            anchors.centerIn: parent
            font {
                pixelSize: facade.doPx(30);
                family:trebuchetMsNorm.name
            }
            text: {
             loader.allCityModel[loader.curCityIndex].target0
            }
        }
    }
}
