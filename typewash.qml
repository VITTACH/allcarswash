import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    id: rootWashs

    property int inerHeight

    Component.onCompleted: {
        partnerHeader.text = qsTr("Тип мойки")
    }

    ListView {
        width: parent.width
        anchors {
            top: header.bottom
            bottom: parent.bottom
        }
        boundsBehavior: Flickable.StopAtBounds

        model:ListModel {
            id: citiesModel
            ListElement {
                image: "ui/washs/car.png";
                target1: "Легковые";
            }
            ListElement {
                image: "ui/washs/bus.png";
                target1: "Грузовые";
            }
            ListElement {
                image: "ui/washs/you.png";
                target1: "Мой сам";
            }
        }
        delegate: Rectangle {
            width: parent.width
            height: inerHeight + facade.toPx(40)

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    parent.color="#90DDDDDD"
                }
                onReleased:{
                    parent.color = "#FFFFFF"
                }
                onPositionChanged: {
                    parent.color = "#FFFFFF"
                }
                onClicked: {
                    currentButon = index
                    loader.goTo("qrc:/listwashs.qml")
                }
            }

            Item {
                anchors.fill: parent
                Row {
                    height: inerHeight
                    spacing: facade.toPx(20)
                    anchors {
                        left: parent.left
                        leftMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                    Image {
                        id: inerimage
                        Component.onCompleted: inerHeight=height
                        width: facade.toPx(sourceSize.width*1.5)
                        height:facade.toPx(sourceSize.width*1.5)
                        source: image
                    }
                    Text {
                        text: target1
                        color: "#3589E9"
                        font {
                            family:trebuchetMsNorm.name
                            pixelSize: facade.doPx(24);
                        }
                    }
                }
                Image {
                    source: "ui/moreIcons/next.png"
                    width: facade.toPx(sourceSize.width * 1.2)
                    height:facade.toPx(sourceSize.height* 1.2)
                    anchors {
                        right: parent.right
                        rightMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    width: parent.width
                    color: "#DDDDDD"
                    height: 1
                }
            }
        }
    }

    Rectangle {
        id: header
        width: parent.width
        height:facade.toPx(120)
        anchors {
            top: parent.top
            topMargin: partnerHeader.height - facade.toPx(10)
        }
        Rectangle {
            height: 1
            color: "#DDDDDD"
            width: parent.width
            anchors.bottom: {parent.bottom}
        }
        Text {
            id: inerText
            anchors.centerIn: parent
            font {
                pixelSize: facade.doPx(30);
                family:trebuchetMsNorm.name
            }
            text: {
             loader.allCityModel[loader.curCityIndex].target0
            }
        }
    }
}
