import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    id: rootMore

    ListView {
        id: listView
        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height
        }

        model:ListModel {
            ListElement {
                image: "ui/moreIcons/allWashes.png"
                target0: "Все мойки"
            }
            ListElement {
                image: "ui/moreIcons/qrCoupon.png"
                target0: "qR-купоны"
            }
            ListElement {
                image: "ui/moreIcons/allSales.png"
                target0: "Заработай"
            }
            ListElement {
                image: "ui/moreIcons/allNews.png"
                target0: "Новости"
            }
            /*
            ListElement {
                image: "ui/moreIcons/allNews.png"
                target0: "Все сообщения"
            }
            */
            ListElement {
                image: "ui/moreIcons/developers.png"
                target0: "О приложении"
            }
            ListElement {
                image: "ui/moreIcons/socialMedia.png"
                target0: "Мы в соц. сети"
            }
            ListElement {
                image: "ui/moreIcons/FAQuestion.png"
                target0: "F.A.Q."
            }
        }
        delegate: Component {
            Rectangle {
                width: parent.width
                height:Math.max(bug.height, text1.implicitHeight) + facade.toPx(40)

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        switch(index) {
                            case 0:
                                loader.goTo("qrc:/typewash.qml")
                                break
                            case 1:
                                loader.goTo("qrc:/coupons.qml");
                                break
                            case 2: loader.goTo("qrc:/earn.qml")
                                break
                            case 3: loader.goTo("qrc:/news.qml")
                                break
                            case 4:loader.goTo("qrc:/about.qml")
                                break
                            case 5:
                                loader.goTo("qrc:/socmedia.qml")
                                break
                            case 6: loader.goTo("qrc:/faq.qml");
                        }
                    }
                    onPressed: {
                        parent.color="#90DDDDDD"
                    }
                    onReleased: {
                        parent.color = "#FFFFFF"//(index%2==0? "#F5F5F5":"#E0E0E0")
                    }
                    onPositionChanged: {
                        parent.color = "#FFFFFF"//(index%2==0? "#F5F5F5":"#E0E0E0")
                    }
                }

                Item {
                    anchors.fill: parent
                    anchors.leftMargin: facade.toPx(20);

                    Text {
                        id: text1
                        color: "#358BE6"
                        wrapMode: Text.Wrap
                        width: parent.width
                        font.family:trebuchetMsNorm.name
                        font.pixelSize: facade.doPx(24);
                        text: target0

                        anchors {
                            left: parent.left
                            leftMargin: facade.toPx(100);
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    Image {
                        id: bug
                        width: facade.toPx(sourceSize.width * 1.5)
                        height:facade.toPx(sourceSize.height* 1.5)
                        source: image
                        anchors {
                            verticalCenter: parent.verticalCenter
                        }
                    }
                    Image {
                        source: "ui/moreIcons/next.png"
                        width: facade.toPx(sourceSize.width * 1.2)
                        height:facade.toPx(sourceSize.height* 1.2)
                        anchors {
                            right: parent.right
                            rightMargin: facade.toPx(40)
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
                Rectangle {
                    width: parent.width
                    height: 1
                    color: "lightgray"
                    anchors.bottom: parent.bottom
                }
            }
        }
    }

    Component.onCompleted: partnerHeader.text = qsTr("Еще")
}
