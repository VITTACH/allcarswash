import QtQuick 2.7
import QtLocation 5.6
import QtPositioning 5.6
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Item {
    id: rootMap

    Plugin {
        name: "osm"
        id: osmPlugin
    }

    Timer {
        id: timer
        repeat: true
        running: true
        interval: 100
        onTriggered: {
            if(listView.y <= rootMap.height/2) showMore();
            if(listView.y == oldY && rolingup) showMore();
        }
    }

    property int ind

    property real oldY: rootMap.height - facade.toPx(150);

    property real oldContY

    property bool rolingup

    function showMore() {
        rotates.restart()
        if(rolingup==false){
            touchArea.visible= false
            timer.stop();
        } else {
            timer.start()
            touchArea.visible= true;
        }
        rolingup= !rolingup;
    }

    function realodWashes(){
        washesModel.clear();
        busyIndicator.visible = true
        var arr= loader.allWashModel
        arr.forEach(function(item,i,arr) {
            if (currentButon==item.type)
                washesModel.append({
                vis: true,
                    image: item.image,
                        target0: item.target0,
                            target1: item.target1,
                                target2: item.target2,
                                    target3: item.target3
                });
        });
        washsModel.positionViewAtBeginning()
        oldContY=washsModel.contentY
        busyIndicator.visible =false
    }

    Map {
        id: map
        zoomLevel: 10
        plugin:osmPlugin
        width: parent.width
        anchors.fill:parent

        function setPosition() {
            if(loader.curWashIndex>-1){
                var position=allWashModel[curWashIndex].coordinate;
                map.center=position;
                map.setWay(position)
                map.zoomLevel=16
            }
            else
                map.center=locationOslo
        }

        function setWay(positionPint) {
            routeQuery.clearWaypoints()
            routeQuery.addWaypoint(locationOslo)
            routeQuery.addWaypoint(positionPint)
            routeQuery.travelModes = RouteQuery.CarTravel
            routeQuery.routeOptimizations = RouteQuery.FastestRoute

            routeModel.update();
        }

        Repeater {
            model: allWashModel;

            delegate: MapQuickItem {
                visible: (currentButon == modelData.type)? (true): (false)

                anchorPoint.y: {image.height}
                anchorPoint.x: {image.width*0.5}
                coordinate: modelData.coordinate

                sourceItem:Column {
                    Image {
                        id: image
                        MouseArea {
                            onClicked: {
                                /*
                                if(image.scale==1)
                                    image.scale= 1.5
                                else
                                    image.scale= 1
                                    */
                                var positon=allWashModel[index].coordinate
                                curWashIndex=index;
                                map.center=positon;
                                map.setWay(positon)
                                map.zoomLevel=16
                            }
                            anchors.fill: parent
                        }
                        source: currentButon==0?
                                    "ui/map/pointerblue.png":
                                    (currentButon == 1?
                                         "ui/map/pointerred.png":
                                                "ui/map/pointergren.png");
                    }
                    /*
                    Text {text: modelData.name;}
                    */
                }
            }
        }

        RouteModel {
            id: routeModel
            plugin: osmPlugin
            query:RouteQuery{
                id:routeQuery
            }
        }

        MapItemView {
            model: routeModel
            delegate: {
                routeDelegate
            }
        }

        Component {
            id: routeDelegate
            MapRoute {
                route: {
                    routeData
                }
                line.color: currentButon==0?"#5BB1FF":(currentButon==1?"red":"green")
                line.width: 6
                smooth: true;
                opacity: 0.5;
            }
        }
    }

    Rectangle {
        y: oldY
        clip: true
        id: listView
        color: "#C4FFFFFF"
        width: parent.width
        height: facade.toPx(150)+oldY-y;

        Item {
            id: card
            visible: false;
            y: facade.toPx(20)
            width:parent.width
            height: facade.toPx(300)

            Rectangle {
                color: "transparent";
                width: {parent.width}
                height:facade.toPx(5)
                anchors.bottom: centerRow2.top
                LinearGradient {
                    anchors.fill:parent;
                    start:Qt.point(0, 0)
                    end:  Qt.point(0, parent.height)
                    gradient: Gradient {
                        GradientStop {
                            position: 0; color: "#00000000";
                        }
                        GradientStop {
                            position: 1; color: "#40000000";
                        }
                    }
                }
            }

            Rectangle {
                id: centerRow2
                color: "#FFFFFF"
                width: parent.width
                height: parent.height

                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        parent.color="#EEEEEE"
                    }
                    onReleased:{
                        parent.color = "#FFFFFF"
                    }
                    onPositionChanged: {
                        parent.color = "#FFFFFF"
                    }
                    onClicked: {
                        curWashIndex=ind
                        loader.goTo("qrc:/wahsinfo.qml")
                    }
                }

                Item {
                    anchors.fill: parent
                    Item {
                        height: bug2.height
                        width: parent.width
                        anchors {
                            left:parent.left
                            leftMargin: facade.toPx(40);
                            verticalCenter: parent.verticalCenter
                        }

                        Item {
                            id: item2
                            width:bug2.width
                            Rectangle {
                                id: bug2
                                clip: true
                                smooth: true
                                visible: false
                                width: facade.toPx(200);
                                height:facade.toPx(200);
                                Image {
                                    id: cardImage
                                    height:sourceSize.width > sourceSize.height? facade.toPx(200):sourceSize.height * (parent.width/sourceSize.width)
                                    width: sourceSize.width > sourceSize.height? sourceSize.width*(parent.height/sourceSize.height): facade.toPx(200)
                                }
                            }

                            Image {
                                id: mask2
                                smooth: true
                                visible: false
                                source: "ui/uimask/roundMask.png"
                                sourceSize: Qt.size(facade.toPx(300), facade.toPx(300))
                            }

                            OpacityMask {
                                source: bug2
                                maskSource: mask2
                                anchors.fill:bug2
                            }
                        }

                        Column {
                            id: inerColumn2
                            anchors {
                            left: item2.right
                            leftMargin: facade.toPx(40)
                            verticalCenter:parent.verticalCenter
                            }
                            width: parent.width/2
                            spacing: facade.toPx(20)
                            Text {
                                id: cardTarget0
                                color: "#000000"
                                width: parent.width
                                elide: Text.ElideRight
                                font {
                                    family: trebuchetMsNorm.name
                                    pixelSize: {facade.doPx(24)}
                                }
                            }
                            Row {
                                width: parent.width
                                spacing: facade.toPx(20)
                                Image {
                                    source:"ui/washs/icons/pointIcon.png"
                                    height:facade.toPx(sourceSize.height*1.5)
                                    width: facade.toPx(sourceSize.width *1.5)
                                }
                                Text {
                                    id: cardTarget1
                                    color: "#3589E9"
                                    width: parent.width
                                    elide: Text.ElideRight
                                    font {
                                        family:trebuchetMsNorm.name
                                        pixelSize: facade.doPx(20);
                                    }
                                }
                            }
                            Row {
                                width: parent.width
                                spacing: facade.toPx(20)
                                Image {
                                    source: "ui/washs/icons/waysIcon.png"
                                    height:facade.toPx(sourceSize.height*1.5)
                                    width: facade.toPx(sourceSize.width *1.5)
                                }
                                Text {
                                    id: cardTarget2
                                    color: "#3589E9"
                                    width: parent.width
                                    elide: Text.ElideRight
                                    font {
                                        family:trebuchetMsNorm.name
                                        pixelSize: facade.doPx(20);
                                    }
                                }
                            }
                            Row {
                                width: parent.width
                                spacing: facade.toPx(20)
                                Image {
                                    source:"ui/washs/icons/priceIcon.png"
                                    height:facade.toPx(sourceSize.height*1.5)
                                    width: facade.toPx(sourceSize.width *1.5)
                                }
                                Text {
                                    id: cardTarget3
                                    color: "#3589E9"
                                    width: parent.width
                                    elide: Text.ElideRight
                                    font {
                                        family:trebuchetMsNorm.name
                                        pixelSize: facade.doPx(20);
                                    }
                                }
                            }
                        }
                    }
                    Image {
                        source: "ui/moreIcons/next.png"
                        width: facade.toPx(sourceSize.width * 1.2)
                        height:facade.toPx(sourceSize.height* 1.2)
                        anchors {
                            right: parent.right
                            rightMargin:facade.toPx(40)
                            verticalCenter: parent.verticalCenter;
                        }
                    }
                }
            }

            Rectangle {
                color: "transparent";
                width: {parent.width}
                height:facade.toPx(5)
                anchors.top: centerRow2.bottom
                LinearGradient {
                    anchors.fill:parent;
                    start:Qt.point(0, 0)
                    end:  Qt.point(0, parent.height)
                    gradient: Gradient {
                        GradientStop {
                            position: 0; color: "#40000000"
                        }
                        GradientStop {
                            position: 1; color: "#00000000"
                        }
                    }
                }
            }
        }

        ListView {
            id: washsModel;
            anchors {
                fill: parent
                topMargin:facade.toPx(20)
            }

            spacing: facade.toPx(15)

            boundsBehavior: {Flickable.StopAtBounds}
            onContentYChanged: {
                ind = Math.floor(contentY/(card.height + facade.toPx(20)))
                if (contentY >oldContY) {
                    washesModel.get(ind).vis = false
                    cardTarget0.text=washesModel.get(ind).target0
                    cardTarget1.text=washesModel.get(ind).target1
                    cardTarget2.text=washesModel.get(ind).target2 + "км. "
                    cardTarget3.text=washesModel.get(ind).target3
                    cardImage.source=washesModel.get(ind).image
                    if(ind == 0)card.visible = true;
                }
                else {
                    washesModel.get(ind+1).vis= true
                    cardTarget0.text=washesModel.get(ind).target0
                    cardTarget1.text=washesModel.get(ind).target1
                    cardTarget2.text=washesModel.get(ind).target2 + "км. "
                    cardTarget3.text=washesModel.get(ind).target3
                    cardImage.source=washesModel.get(ind).image
                }
                oldContY = contentY;
            }

            onAtYBeginningChanged: {
                if(atYBeginning==true){
                    if(!touchArea.visible ==true)
                    {
                        listView.y += 1
                        timer.restart()
                        touchArea.visible = true;
                    }
                }
            }

            Component.onCompleted: realodWashes()

            model:ListModel {
                id: washesModel;
                ListElement {
                    vis: true
                    image: "";
                    target0: "";
                    target1: "";
                    target2: "";
                    target3: "";
                }
            }
            delegate: Item {
                visible: vis
                width: {listView.width}
                height:facade.toPx(300)

                Rectangle {
                    color: "transparent";
                    width: {parent.width}
                    height:facade.toPx(5)
                    anchors.bottom: centerRow.top
                    LinearGradient {
                        anchors.fill:parent;
                        start:Qt.point(0, 0)
                        end:  Qt.point(0, parent.height)
                        gradient: Gradient {
                            GradientStop {
                                position: 0; color: "#00000000";
                            }
                            GradientStop {
                                position: 1; color: "#40000000";
                            }
                        }
                    }
                }

                Rectangle {
                    id: centerRow
                    color: "#FFFFFF"
                    width: parent.width
                    height: parent.height

                    MouseArea {
                        anchors.fill: parent
                        onPressed: {
                            parent.color = "#EEEEEE"
                        }
                        onReleased:{
                            parent.color = "#FFFFFF"
                        }
                        onPositionChanged: {
                            parent.color = "#FFFFFF"
                        }
                        onClicked: {
                            curWashIndex=index
                            loader.goTo("qrc:/wahsinfo.qml")
                        }
                    }

                    Item {
                        anchors.fill: parent
                        Item {
                            height: bug.height
                            width: parent.width
                            anchors {
                                left:parent.left
                                leftMargin: facade.toPx(40);
                                verticalCenter: parent.verticalCenter
                            }

                            Item {
                                id: item
                                width: bug.width
                                Rectangle {
                                    id: bug
                                    clip: true
                                    smooth: true
                                    visible: false
                                    width: facade.toPx(200);
                                    height:facade.toPx(200);
                                    Image {
                                        source: image
                                        height:sourceSize.width > sourceSize.height? facade.toPx(200):sourceSize.height * (parent.width/sourceSize.width)
                                        width: sourceSize.width > sourceSize.height? sourceSize.width*(parent.height/sourceSize.height): facade.toPx(200)
                                    }
                                }

                                Image {
                                    id: mask
                                    smooth: true
                                    visible: false
                                    source: "ui/uimask/roundMask.png"
                                    sourceSize: Qt.size(facade.toPx(300), facade.toPx(300))
                                }

                                OpacityMask {
                                    source: bug
                                    maskSource: mask
                                    anchors.fill: bug
                                }
                            }

                            Column {
                                id: inerColumn
                                anchors {
                                    left: item.right
                                    leftMargin: facade.toPx(40)
                                    verticalCenter: parent.verticalCenter
                                }
                                width: parent.width/2
                                spacing: facade.toPx(20)
                                Text {
                                    text: target0
                                    color: "#000000"
                                    width: parent.width
                                    elide: Text.ElideRight
                                    font {
                                        family:trebuchetMsNorm.name
                                        pixelSize: facade.doPx(24);
                                    }
                                }
                                Row {
                                    width: parent.width
                                    spacing: facade.toPx(20)
                                    Image {
                                        source:"ui/washs/icons/pointIcon.png"
                                        height:facade.toPx(sourceSize.height*1.5)
                                        width: facade.toPx(sourceSize.width *1.5)
                                    }
                                    Text {
                                        text: target1
                                        color: "#3589E9"
                                        width: parent.width
                                        elide: Text.ElideRight
                                        font {
                                            family:trebuchetMsNorm.name
                                            pixelSize: facade.doPx(20);
                                        }
                                    }
                                }
                                Row {
                                    width: parent.width
                                    spacing: facade.toPx(20)
                                    Image {
                                        source: "ui/washs/icons/waysIcon.png"
                                        height:facade.toPx(sourceSize.height*1.5)
                                        width: facade.toPx(sourceSize.width *1.5)
                                    }
                                    Text {
                                        text: target2 + "км. "
                                        color: "#3589E9"
                                        width: parent.width
                                        elide: Text.ElideRight
                                        font {
                                            family:trebuchetMsNorm.name
                                            pixelSize: facade.doPx(20);
                                        }
                                    }
                                }
                                Row {
                                    width: parent.width
                                    spacing: facade.toPx(20)
                                    Image {
                                        source:"ui/washs/icons/priceIcon.png"
                                        height:facade.toPx(sourceSize.height*1.5)
                                        width: facade.toPx(sourceSize.width *1.5)
                                    }
                                    Text {
                                        text: target3
                                        color: "#3589E9"
                                        width: parent.width
                                        elide: Text.ElideRight
                                        font {
                                            family:trebuchetMsNorm.name
                                            pixelSize: facade.doPx(20);
                                        }
                                    }
                                }
                            }
                        }
                        Image {
                            source: "ui/moreIcons/next.png"
                            width: facade.toPx(sourceSize.width * 1.2)
                            height:facade.toPx(sourceSize.height* 1.2)
                            anchors {
                                right: parent.right
                                rightMargin:facade.toPx(40)
                                verticalCenter: parent.verticalCenter;
                            }
                        }
                    }
                }

                Rectangle {
                    color: "transparent";
                    width: {parent.width}
                    height:facade.toPx(5)
                    anchors.top: centerRow.bottom
                    LinearGradient {
                        anchors.fill:parent;
                        start:Qt.point(0, 0)
                        end:  Qt.point(0, parent.height)
                        gradient: Gradient {
                            GradientStop {
                                position: 0; color: "#40000000"
                            }
                            GradientStop {
                                position: 1; color: "#00000000"
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        color: "transparent"
        width: parent.width;
        height: facade.toPx(5)
        anchors.bottom: listView.top
        LinearGradient {
            anchors.fill:parent
            start:Qt.point(0, 0)
            end:  Qt.point(0, parent.height)
            gradient: Gradient {
                GradientStop {
                    position: 0; color: "#00000000";
                }
                GradientStop {
                    position: 1; color: "#40000000";
                }
            }
        }
    }

    MouseArea {
        id: touchArea
        propagateComposedEvents: true;
        width: parent.width
        drag.target: listView
        drag.axis: Drag.YAxis
        drag.minimumY: washsModel.count*listView.height > rootMap.height/2? rootMap.height/2: washsModel.count*listView.height
        drag.maximumY: rootMap.height - listView.height + oldY - listView.y;
        anchors {
            top:parent.bottom
            bottom:parent.bottom
            topMargin:-listView.height
        }
    }

    Rectangle {
        clip: true
        color: "white"
        width:parent.width
        height: facade.toPx(300)
        anchors.bottom: downRow.top
        visible:curWashIndex>-1?1:0

        border {
            width: 1
            color: "lightgray";
        }

        Column {
            anchors {
                top: parent.top
                left:parent.left
                right: parent.right
                topMargin: facade.toPx(20);
                leftMargin: facade.toPx(20)
                rightMargin:facade.toPx(20)
            }
            height: {parent.height}
            Flickable {
                width: {parent.width - facade.doPx(50);}
                height: {parent.height - facade.doPx(50);}
                flickableDirection:Flickable.VerticalFlick
                TextArea.flickable: TextArea {
                wrapMode: Text.Wrap
                readOnly: true;
                color:"#000000"
                font {
                    pixelSize: facade.doPx(22);
                    family:trebuchetMsNorm.name
                }
                text: curWashIndex > -1?
                        "Автомойка: " + allWashModel[curWashIndex].target0 +
                            "\nАдрес: " + allWashModel[curWashIndex].target1 +
                                "\nРасстояние: " + allWashModel[curWashIndex].target2 + " км." +
                                    "\nТелефон: " + allWashModel[curWashIndex].phone.substring(0,2) + " (" + allWashModel[curWashIndex].phone.substring(2,5) + ") " + allWashModel[curWashIndex].phone.substring(5,8) + "-" + allWashModel[curWashIndex].phone.substring(8,10) + "-" + allWashModel[curWashIndex].phone.substring(10) +
                                        "\nРабочее время: " + allWashModel[curWashIndex].time:""
                }
            }

            Button {
                text: "Подробнее"
                height: {facade.toPx(35)}

                background:Rectangle{opacity:0}
                font {
                    pixelSize: facade.doPx(22);
                    family:trebuchetMsNorm.name
                    underline: true
                }

                onClicked: loader.goTo("qrc:/wahsinfo.qml")

                contentItem: Text {
                    color:"#348AE6"
                    verticalAlignment: {
                        Text.AlignVCenter
                    }
                    horizontalAlignment:{
                        Text.AlignHCenter
                    }
                    elide:Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }
        }

        Button {
            anchors {
                top: parent.top
                right: parent.right;
                topMargin: {
                    facade.toPx(40);
                }
                rightMargin: {
                    facade.toPx(40);
                }
            }

            onClicked: {
                curWashIndex=-1
                map.setWay(locationOslo)
            }
            background: Image {
                id: closeimage;
                width:facade.toPx(sourceSize.width*2)
                height:{
                    facade.toPx(sourceSize.height *2)
                }
                source: "ui/buttons/closeButton.png";
            }
            width: closeimage.width;
            height:closeimage.height
        }
    }

    Row {
        id: downRow
        anchors.bottom:parent.bottom
        Repeater {
            delegate: Button {
                text:modelData

                font {
                pixelSize: {facade.doPx(22)}
                family: trebuchetMsNorm.name
                }

                width: rootMap.width/3;
                height:facade.toPx(130)

                background: Rectangle {
                    color: parent.down? "#F45BB1FF": (currentButon == index? "#F45BB1FF": "#F43383DF")
                }

                onClicked: {
                    curWashIndex = -1
                    currentButon = index
                    map.setWay(locationOslo)
                    map.zoomLevel= 10
                    realodWashes();
                    listView.y = oldY
                    timer.restart()
                    touchArea.visible = true
                }

                contentItem: Text {
                    color: "white"
                    horizontalAlignment:{
                        Text.AlignHCenter
                    }verticalAlignment: {
                        Text.AlignVCenter
                    }
                    elide:Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }
            model:["Легковые","Грузовые","Мой Сам"]
        }
    }

    Button {
        id: mores
        onClicked: {
            timer.restart()
            transit.start()
            curWashIndex=-1
        }
        width: moreImage.width;
        height:moreImage.height
        anchors {
        bottom: listView.top
        bottomMargin: -height/2 - facade.toPx(20)/2
        horizontalCenter: {parent.horizontalCenter}
        }
        background:Image {
        id: moreImage;
        fillMode: Image.PreserveAspectFit
        source: "ui/buttons/showMore.png"
        width: facade.toPx(sourceSize.width * 1.2);
        height:facade.toPx(sourceSize.height* 1.2);
        }
    }
    Image {
        id: vector;
        anchors.centerIn: mores
        source: "ui/buttons/vectorMore.png"
        width: facade.toPx(sourceSize.width * 1.2);
        height:facade.toPx(sourceSize.height* 1.2);
    }
    RotationAnimator {
        id: rotates
        from: rolingup? 180: 0;
        to: rolingup? 0:180
        target:vector
        duration: 400
    }
    PropertyAnimation {
        id: transit
        target:listView
        from:rolingup?(listView.y>rootMap.height/2&&rolingup? listView.y: rootMap.height/2):oldY
        to: rolingup? oldY:(listView.y>rootMap.height/2&&rolingup? listView.y: rootMap.height/2)
        property: "y"
        duration: 400
    }

    Component.onCompleted: {
        partnerHeader.text = qsTr("Поиск");
        map.setPosition()
    }
}
