#include "eventhandler.h"

#include <QDir>
#include <QClipboard>
#include <QApplication>
#include <QStandardPaths>
#include <QCoreApplication>
#ifdef Q_OS_ANDROID
    #include <QtAndroidExtras/QAndroidJniObject>
#endif

int EventHandler::currentOSys() {
    return currentSys;
}

void EventHandler::copyText(QString text) {
     QClipboard *clipboard = QApplication::clipboard();
     clipboard->clear();
     clipboard->setText(text);
}

void EventHandler::openingMap(QString lat, QString lon)
{
    qDebug()<<"opening Map on pos= " + lat + " " + lon;
#ifdef Q_OS_ANDROID
    QAndroidJniObject mat = QAndroidJniObject::fromString(lat);
    QAndroidJniObject mon = QAndroidJniObject::fromString(lon);
    QAndroidJniObject::callStaticMethod<void>("quickandroid/QuickAndroidActivity", "openingMap", "(Ljava/lang/String;Ljava/lang/String;)V", mat.object<jstring>(), mon.object<jstring>());
#endif
}

EventHandler::EventHandler(QObject * parent): QObject(parent) {
    if(!currentSys) {
        QString path = QStandardPaths::standardLocations(QStandardPaths::DataLocation).value(0);
        QDir dir(path);
        if(!dir.exists())
            dir.mkpath(path);
        if(!path.isEmpty()&&!path.endsWith("/"))
            path += "/" + PathFile;
            PathFile = path;
    }
}
