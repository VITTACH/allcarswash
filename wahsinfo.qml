import QtQuick 2.7
import QtLocation 5.6
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Item {
    id: rootAbout

    property int pls: 0
    property int ind:-1
    property int imageIndx:-1
    property int pageWidth: 0
    property int pageHeigh: 0

    Rectangle {
        color: "#F6F6F6";
        anchors.fill: parent;
    }

    Timer {
        id:calc
        interval: 1;
        onTriggered: {
            pageHeigh += pls
        }
    }

    Component.onCompleted: {
        ind = identifyName()
        partnerHeader.text = qsTr("Подробнее")
    }

    function identifyName(){
        var count = 0
        var index = 0
        for(var i=0;i<allWashModel.length;i++)
        if(allWashModel[i].type==currentButon)
        {
            index = i
            if (count === curWashIndex) break;
            count++
        }
        curWashIndex=index;
        return index;
    }

    ListView {
        id: listView;

        width: parent.width
        anchors {
            top: parent.top
            bottom: downRow.top
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        delegate: Rectangle {
            height: pageHeigh
            width: parent.width
            color: "transparent"
            Item {
                Item {
                    z: 1
                    id: itemNum0
                    width: parent.width
                    height: facade.toPx(400)

                    Component.onCompleted: {
                        pageHeigh += height;
                    }

                    Plugin {name: "osm"; id: osmPlugin}

                    Map {
                        id: map
                        zoomLevel: 13
                        plugin: osmPlugin
                        center: allWashModel[ind].coordinate
                        anchors.fill: parent

                        MapQuickItem {
                            anchorPoint {
                                y: pointerImage.height;
                                x: pointerImage.width * 0.5;
                            }
                            coordinate: {
                                allWashModel[ind].coordinate
                            }
                            sourceItem: Image {
                                id:pointerImage
                                source: (currentButon == 0)? "ui/map/pointerblue.png": (currentButon == 1? "ui/map/pointerred.png": "ui/map/pointergren.png")
                            }
                        }
                    }
                }

                Column {
                    z: 2
                    id: itemNum1
                    width: parent.width
                    anchors {
                        topMargin: -height + facade.toPx(8)
                        top:itemNum0.bottom
                    }

                    Rectangle {
                        width: parent.width
                        height: facade.toPx(135)

                        Label {
                            anchors {
                                left: parent.left;
                                leftMargin: facade.toPx(20)
                                verticalCenter: parent.verticalCenter
                            }
                            font {
                                pixelSize: facade.doPx(22);
                                family:trebuchetMsNorm.name
                                // underline: true
                            }
                            text: allWashModel[ind].target0
                        }

                        Button {
                            anchors {
                                right:parent.right
                                rightMargin:facade.toPx(30)
                                verticalCenter: parent.verticalCenter
                            }
                            onClicked:loader.goTo("qrc:/mapview.qml")
                            width: {backimage.width;}
                            height:{backimage.height}
                            background: Image {
                            id: backimage
                            source:"ui/map/karta.png"
                            width: facade.toPx(sourceSize.width *1.5)
                            height:facade.toPx(sourceSize.height*1.5)
                            }
                        }
                    }
                    Rectangle {
                        color: "transparent"
                        width: parent.width;
                        height: facade.toPx(8)

                        Component.onCompleted: {
                            pageHeigh += height;
                        }

                        LinearGradient {
                            anchors.fill: parent
                            start:Qt.point(0, 0)
                            end:  Qt.point(0, parent.height)
                            gradient: Gradient {
                                GradientStop {
                                    position: 0; color: "#25000000";
                                }
                                GradientStop {
                                    position: 1; color: "#01000000";
                                }
                            }
                        }
                    }
                }

                Item {
                    z: 3
                    id: itemNum3
                    width: bug.width
                    anchors {
                        left: parent.left
                        bottom: itemNum1.top
                        bottomMargin: 5*width/6
                        leftMargin: facade.toPx(20);
                    }

                    Rectangle {
                        id: bug
                        clip: true
                        smooth: true
                        visible: false
                        width: facade.toPx(250)
                        height:facade.toPx(250)
                        Image {
                            source:allWashModel[ind].image
                            height:sourceSize.width>sourceSize.height? facade.toPx(250):sourceSize.height * (parent.width/sourceSize.width)
                            width: sourceSize.width>sourceSize.height? sourceSize.width*(parent.height/sourceSize.height): facade.toPx(250)
                        }
                    }

                    Image {
                        id: mask
                        smooth: true
                        visible: false
                        source: "ui/uimask/roundMask.png"
                        sourceSize: Qt.size(facade.toPx(250), facade.toPx(250))
                    }

                    OpacityMask {
                        source: bug
                        maskSource: mask
                        anchors.fill: bug
                    }

                    Rectangle {
                        x: -4
                        y: -4
                        radius:width * 0.5
                        height:bug.height+ 8
                        width: bug.width + 8
                        color: "transparent"
                        border {
                            width: 5
                            color:"#40000000"
                        }
                    }
                }

                ListView {
                    id: navPhotos

                    height:facade.toPx(300)
                    spacing:facade.toPx(10)

                    Component.onCompleted:
                    {
                        pageHeigh+= height;
                        photoModel.clear();
                        ind=identifyName();
                        for(var i=0; i < allWashModel[ind].picture.length; i++)
                        photoModel.append({image:allWashModel[ind].picture[i]})
                    }

                    anchors {
                    top: itemNum1.bottom
                    left: {parent.left;}
                    right:{parent.right}
                    leftMargin:facade.toPx(20)
                    }
                    orientation: Qt.Horizontal

                    model:ListModel {
                        id:photoModel
                        ListElement {image:""}
                    }
                    delegate: Rectangle {
                        MouseArea {
                            anchors.fill: parent;
                            onClicked: {
                                imageIndx = index
                                loader.webview = true
                            }
                        }
                        clip: true
                        color: "transparent"
                        width: {facade.toPx(250)}
                        height:{facade.toPx(250)}
                        anchors.bottom: parent.bottom
                        Image {
                            source:image
                            anchors.centerIn: parent;
                            height:sourceSize.width > sourceSize.height? facade.toPx(250): sourceSize.height*(parent.width/sourceSize.width)
                            width: sourceSize.width > sourceSize.height? sourceSize.width*(parent.height/sourceSize.height):facade.toPx(250)
                        }
                    }
                }

                Item {
                    id: itemNum2
                    width: parent.width
                    anchors.top: navPhotos.bottom
                    height:{
                        pls=Math.max(info.height,description.contentHeight)
                    }
                    Column {
                        id: info
                        height: text1.implicitHeight + text2.implicitHeight
                        anchors {
                            top: parent.top
                            left: parent.left
                            topMargin: {facade.toPx(40)}
                            leftMargin:{facade.toPx(20)}
                        }

                        Label {
                            id: text1
                            text: "График работы: "
                            font {
                                pixelSize: facade.doPx(22);
                                family:trebuchetMsNorm.name
                            }
                        }
                        Label {
                            id: text2
                            color: "#348AE6"
                            text: allWashModel[ind].time
                            font {
                                family:trebuchetMsNorm.name
                                pixelSize: facade.doPx(28);
                            }
                        }
                    }

                    Label {
                        Component.onCompleted: calc.start()
                        id: description
                        text: allWashModel[ind].desc
                        anchors {
                            top: parent.top
                            right: parent.right
                            topMargin: {facade.toPx(40)}
                            rightMargin: facade.toPx(20)
                        }
                        font {
                            pixelSize: {facade.doPx(28)}
                            family: trebuchetMsNorm.name
                        }
                        lineHeight:2
                    }
                }

                ListView {
                    id: itemNum4

                    height: facade.toPx(80)
                    spacing:facade.toPx(30)

                    Component.onCompleted: {pageHeigh += height; loaderTime.restart()}

                    Timer {
                        id:loaderTime;
                        interval: 1000
                        onTriggered: {
                            iconsModel.clear()

                            if(allWashModel[ind].desc.toLowerCase().search("тел")>0) {
                                iconsModel.append({image: "ui/washs/icons/tv.png"});
                            }
                            if(allWashModel[ind].desc.toLowerCase().search("ком")>0) {
                                iconsModel.append({image: "ui/washs/icons/bed.png"})
                            }
                            if(allWashModel[ind].desc.toLowerCase().search("каф")>0) {
                                iconsModel.append({image:"ui/washs/icons/cafe.png"})
                            }
                            if(allWashModel[ind].desc.toLowerCase().search("wi") >0) {
                                iconsModel.append({image:"ui/washs/icons/wifi.png"})
                            }
                            if(allWashModel[ind].desc.toLowerCase().search("туа")>0) {
                                iconsModel.append({image:"ui/washs/icons/toilet.png"})
                            }
                            if(allWashModel[ind].desc.toLowerCase().search("коф")>0) {
                                iconsModel.append({image:"ui/washs/icons/coffee.png"})
                            }
                        }
                    }

                    orientation: Qt.Horizontal
                    anchors {
                        left: parent.left
                        right: parent.right;
                        top: {itemNum2.bottom}
                        topMargin: -facade.toPx(50)
                        leftMargin: parent.width > facade.toPx(137)*iconsModel.count? (parent.width - facade.toPx(137)*iconsModel.count)/2:0
                    }

                    model:ListModel {
                        id:iconsModel
                        ListElement {
                            image: ""
                        }
                    }
                    delegate: Image {
                        source: image
                        height:facade.toPx(sourceSize.height* 1.5)
                        width: facade.toPx(sourceSize.width * 1.5)
                    }
                }
                anchors.fill: parent
            }

            Rectangle {
                id: showImage
                color: {"#C0FFFFFF"}
                width: parent.width;
                anchors.fill: parent
                visible: loader.webview
                Button {
                    anchors.fill:parent
                    background: Rectangle {
                        id: photoFrame;
                        color:"transparent"
                        Behavior on scale {
                            NumberAnimation {duration: 200;}
                        }
                        Behavior on x {
                            NumberAnimation {duration: 200;}
                        }
                        Behavior on y {
                            NumberAnimation {duration: 200;}
                        }
                        PinchArea {
                            pinch.dragAxis:{Pinch.XAndYAxis}
                            anchors.fill:parent
                            pinch.target:photoFrame
                            pinch.minimumScale: Math.min(parent.width,parent.height)/Math.max(image.width,image.height);
                            pinch.maximumScale: 10;
                            MouseArea {
                                id: dragArea
                                hoverEnabled:true
                                anchors.fill:parent
                                drag.target: photoFrame
                                scrollGestureEnabled:false
                                onClicked: {
                                    loader.webview = false
                                }
                                onWheel: {
                                    photoFrame.scale += photoFrame.scale * wheel.angleDelta.y / 120 / 10
                                }
                            }
                            onSmartZoom: {
                                if(pinch.scale>0) {
                                    photoFrame.scale=Math.min(rootAbout.width,rootAbout.height)/Math.max(image.sourceSize.width,image.sourceSize.height)*0.85
                                    photoFrame.x = flick.contentX + (flick.width - photoFrame.width) / 2
                                    photoFrame.y = flick.contentY + (flick.height- photoFrame.height)/ 2
                                } else {
                                    photoFrame.scale=pinch.previousScale
                                    photoFrame.x = pinch.previousCenter.x - photoFrame.width / 2
                                    photoFrame.y = pinch.previousCenter.y - photoFrame.height/ 2
                                }
                            }
                        }
                        Image {
                            id: image
                            anchors.centerIn:parent
                            source: imageIndx > -1? photoModel.get(imageIndx).image:""
                        }
                        Connections {
                            onImageIndxChanged: {
                                photoFrame.x = 0;
                                photoFrame.y = (listView.contentY - (pageHeigh - rootAbout.height) / 2 - downRow.height)
                                photoFrame.scale=Math.min(parent.width,parent.height)/Math.max(image.width,image.height)
                            }
                            target: rootAbout
                        }
                    }
                    onClicked: {
                        loader.webview=false;
                    }
                }
            }
        }
        model: ListModel{ListElement {tr:""}}
        boundsBehavior:Flickable.StopAtBounds
    }

    Rectangle {
        id: downRow
        color: "#3383DF"
        width: parent.width
        height: facade.toPx(120);
        anchors.bottom: parent.bottom
        ListView {
            id: navButtons;
            orientation:Qt.Horizontal
            anchors.horizontalCenter: {parent.horizontalCenter;}

            height: parent.height
            width: (parent.width > facade.toPx(120)*3)? (3*pageWidth): (parent.width)

            model:  ListModel {
                    ListElement {
                        image: "ui/buttons/washs/callButton.png"
                        target1: "Позвонить";
                    }
                    ListElement {
                        image: "ui/buttons/washs/navButton.png";
                        target1: "Навигация";
                    }
                    ListElement {
                        image: "ui/buttons/washs/siteButton.png"
                        target1: "На сайт";
                    }
                }
            delegate: Rectangle {
                height:parent.height
                color: mouseare.pressed? ("#5BB1FF"):("#3383DF")
                width: (pageWidth = (rootAbout.width< facade.toPx(950)?
                                    (rootAbout.width> facade.toPx(120)*3 ?
                                     rootAbout.width: facade.toPx(120)*3):
                                     facade.toPx(950)) / 3) - (index < 3? navButtons.spacing: 0)

                MouseArea {
                    id: mouseare
                    anchors.fill:parent
                    onClicked: {
                        switch(index) {
                        case 0:
                            if (event_handler.currentOSys() == 2)
                                Qt.openUrlExternally("tel:"+ allWashModel[ind].phone)
                            else
                                caller.directCall(allWashModel[ind].phone)
                            break;
                        case 1:
                            var coordinates = allWashModel[ind].coordinate
                            event_handler.openingMap(coordinates.latitude,coordinates.longitude)
                            break;
                        case 2:Qt.openUrlExternally(allWashModel[ind].url)
                            break;
                        }
                    }
                }

                Column {
                    Image {
                        anchors.horizontalCenter: parent.horizontalCenter;
                        width: facade.toPx(sourceSize.width *1.5)
                        height:facade.toPx(sourceSize.height*1.5)
                        source: image
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: facade.toPx(10)
                    Text {
                        text: target1
                        color:"white"
                        horizontalAlignment: {Text.AlignHCenter;}
                        font {
                            family:trebuchetMsNorm.name
                            pixelSize: facade.doPx(16);
                        }
                        width: parent.parent.width
                        wrapMode: Text.Wrap;
                    }
                }
            }
        }
    }
}
