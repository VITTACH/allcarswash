import QtQuick 2.7
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0

Item {
    id: rootShare

    property real pageHeight

    Component.onCompleted: {
        partnerHeader.text = qsTr("Поделись")
    }

    LinearGradient {
        anchors.fill:parent
        start:Qt.point(0, 0)
        end:  Qt.point(0, parent.height)
        gradient: Gradient {
            GradientStop {
                position: 0; color: "#4A9BE9";
            }
            GradientStop {
                position: 1; color: "#247FE5";
            }
        }
    }

    Rectangle {
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height
        }
        width: parent.width
        color: "transparent"
        height: parent.height-partnerHeader.height

        ListView {
            id: listView
            height: pageHeight
            width: parent.width
            boundsBehavior: Flickable.StopAtBounds

            anchors.verticalCenter: parent.verticalCenter;

            model:  ListModel {
                id: profileModel
                    ListElement {
                        image: "ui/headers/share.png";
                        target1: "Поделитесь с друзьями!";
                        target2: "Сделаем наш город\nнемного чище, расскажите\nдрузьям о нашем приложении";
                    }
                }
            delegate: Component {
                Rectangle {
                    color: "transparent"

                    width: parent.width
                    height: (pageHeight =
                            sharing.height +
                            text1.implicitHeight +
                            text2.implicitHeight +
                            imageIcon.height +
                            4*inerColumn.spacing +
                            spacer1.height) +
                            (rootShare.height<
                             sharing.height +
                             text1.implicitHeight +
                             text2.implicitHeight +
                             imageIcon.height +
                             4*inerColumn.spacing +
                             facade.toPx(20)*2+
                             spacer1.height +
                             partnerHeader.height?
                                 sharing.height +
                                 text1.implicitHeight +
                                 text2.implicitHeight +
                                 imageIcon.height +
                                 4*inerColumn.spacing +
                                 spacer1.height +
                                 facade.toPx(20)*2 +
                                 partnerHeader.height -
                                 rootShare.height:0)
                    Column {
                        id: inerColumn
                        width: parent.width
                        spacing: facade.toPx(40)
                        anchors {
                            topMargin: facade.toPx(20)
                            bottomMargin: facade.toPx(20)
                        }

                        anchors.verticalCenter: parent.verticalCenter
                        Image {
                            width: sourceSize.width / 1.5 *
                                   (parent.width<facade.toPx(1024)?
                                        parent.width: facade.toPx(1024))/500;
                            height:sourceSize.height/ 1.5 *
                                   (parent.width<facade.toPx(1024)?
                                        parent.width: facade.toPx(1024))/500;
                            anchors.horizontalCenter: parent.horizontalCenter
                            id: imageIcon
                            source: image
                        }
                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            width: parent.width;
                            height: facade.toPx(20)
                        }
                        Text {
                            id: text1
                            text: target1
                            color: "#FFFFFF"
                            width: parent.width;
                            wrapMode: Text.Wrap;
                            font {
                                bold: true
                                family:trebuchetMsNorm.name
                                pixelSize: facade.doPx(32);
                            }
                            horizontalAlignment: Text.AlignHCenter
                        }
                        Text {
                            id: text2
                            text: target2
                            color: "#FFFFFF"
                            width: parent.width;
                            wrapMode: Text.Wrap;
                            font {
                                family:trebuchetMsNorm.name
                                pixelSize: facade.doPx(24);
                            }
                            horizontalAlignment: Text.AlignHCenter
                        }
                        Button {
                            id: sharing
                            width: facade.toPx(600)
                            height:facade.toPx(110)
                            anchors.horizontalCenter: parent.horizontalCenter

                            text: "Поделиться";

                            onClicked: shareUtils.share("Скачайте приложение 'Все Автомойки' из Google Play","http://bit.ly/1U6qvMT")

                            font {
                                bold: true
                                pixelSize: facade.doPx(22);
                                family:trebuchetMsNorm.name
                            }

                            background: Rectangle {
                                radius:facade.toPx(25);
                                color:parent.down?"#D8D8D8":"#FFFFFF"
                            }

                            contentItem: Text {
                                color:"#000000"
                                horizontalAlignment:Text.AlignHCenter
                                verticalAlignment : Text.AlignVCenter
                                elide:Text.ElideRight
                                text: parent.text
                                font: parent.font
                            }
                        }
                    }
                }
            }
        }
    }
}
