import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    id: rootCoupon
    color: "#90D3FE"

    property int pageHeight
    property bool rolingup;

    Timer {
        id: scrolDown
        interval: 100
        onTriggered: {listView.positionViewAtEnd();}
    }

    ListView {
        id: listView

        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: {
                partnerHeader.height+facade.toPx(30)
            }
        }

        Component.onCompleted: {
            pageHeight=0
            curentCouponModel.clear();
            curentCouponModel.append({
                image_0: loader.couponsModel[loader.curCouponInd].imgdisc,
                    target0: loader.couponsModel[loader.curCouponInd].target0,
                        target1: loader.couponsModel[loader.curCouponInd].target1,
                            target2: loader.couponsModel[loader.curCouponInd].target2,
                                target3: loader.couponsModel[loader.curCouponInd].target3,
                                    target4: loader.couponsModel[loader.curCouponInd].target4
            });
        }

        model: ListModel {
            id: curentCouponModel
            ListElement {
                target2: 0;
                target3: 0;
                image_0: "";
                target0: "";
                target1: "";
                target4: "";
            }
        }
        boundsBehavior: Flickable.StopAtBounds

        delegate: Rectangle {
            height:pageHeight+3*facade.toPx(40);
            width: parent.width
            color: "transparent"
            Column {
                spacing: facade.toPx(40)
                Rectangle {
                    color: "#FFFFFF"
                    width: parent.width
                    Component.onCompleted: {
                        pageHeight += height
                    }
                    height: discountImage.height
                    Image {
                        id: discountImage
                        anchors.centerIn: parent
                        width: facade.toPx(sourceSize.width *1.5)
                        height:facade.toPx(sourceSize.height*1.5)
                        source:image_0
                    }
                    Rectangle {
                        color: "#20000000"
                        width: parent.width
                        height: parent.height/2;
                        anchors {
                            bottom:parent.bottom
                        }
                        Column {
                            width: parent.width;
                            anchors {
                                left:parent.left
                                leftMargin: facade.toPx(50)
                                verticalCenter: parent.verticalCenter
                            }
                            spacing: facade.toPx(10)

                            Text {
                                id: text1
                                color: "#FFFFFF"
                                font {
                                    bold: true
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }
                                styleColor:"black"
                                style: Text.Raised
                                text: target0
                            }
                            Text {
                                id: text2
                                color: "#FFFFFF"
                                font {
                                    bold: true
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }
                                text: target1
                                styleColor:"black"
                                style: Text.Raised
                            }
                        }
                    }
                }
                Rectangle {
                    clip: true
                    color: "#B8E3FE"
                    anchors {
                        left: parent.left
                        leftMargin: facade.toPx(30)
                    }
                    width: parent.width-facade.toPx(60)
                    Component.onCompleted: {
                        pageHeight += height
                    }
                    radius: facade.toPx(25);
                    height: facade.toPx(400)

                    Column {
                        anchors {
                            left:parent.left
                            verticalCenter: parent.verticalCenter;
                            horizontalCenter: parent.horizontalCenter
                        }
                        spacing: facade.toPx(40)

                        Text {
                            color: "#000000"
                            wrapMode: Text.Wrap;
                            font {
                                pixelSize: facade.doPx(24);
                                family:trebuchetMsNorm.name
                            }
                            horizontalAlignment: {Text.AlignHCenter}
                            width: {parent.width - facade.toPx(40);}
                            text: "Вам следует отсканировать код еще "
                                  + (target3 - target2) +
                                  " раз(а), для использования купона."
                            anchors.horizontalCenter: parent.horizontalCenter;
                        }
                        Row {
                            spacing: facade.toPx(200)
                            width: leftRow.width + rightRow.width + spacing;
                            anchors.horizontalCenter: parent.horizontalCenter;
                            height:Math.max(
                                   Math.max(img0.height,text3.implicitHeight),
                                   Math.max(img1.height,text4.implicitHeight))
                            Row {
                                id: leftRow
                                spacing: facade.toPx(20)
                                Image {
                                    id: img0
                                    source: "ui/coupon/lockedIcon.png"
                                    width: facade.toPx(sourceSize.width * 1.5)
                                    height:facade.toPx(sourceSize.height* 1.5)
                                }
                                Text {
                                    id: text3
                                    color: "#000000"
                                    font {
                                        pixelSize: facade.doPx(24);
                                        family:trebuchetMsNorm.name
                                    }
                                    text: target2 + " / " + target3
                                }
                            }
                            Row {
                                id: rightRow
                                spacing: facade.toPx(20)
                                Image {
                                    id: img1
                                    source: "ui/coupon/timerIcon.png"
                                    width: facade.toPx(sourceSize.width * 1.5);
                                    height:facade.toPx(sourceSize.height* 1.5);
                                }
                                Text {
                                    id: text4
                                    color: "#000000"
                                    font {
                                        pixelSize: facade.doPx(24);
                                        family:trebuchetMsNorm.name
                                    }
                                    text: target3 - target2 > 0?
                                              "Сканировать!": "Отсканировано"
                                }
                            }
                        }
                        Row {
                            spacing: target3 - target2 >0? facade.toPx(60): facade.toPx(90)
                            anchors {
                                horizontalCenter: parent.horizontalCenter;
                            }
                            Button {
                                width: facade.toPx(250)
                                height: facade.toPx(90)

                                text: "Скан QR"

                                onClicked: {
                                    if (target3 - target2 >= 1)
                                        loader.goTo("qrc:/qrscaner.qml")
                                }

                                font {
                                    pixelSize: facade.doPx(22);
                                    family:trebuchetMsNorm.name
                                }

                                background: Rectangle {
                                    radius:facade.toPx(15);
                                    color:parent.down?"#DADFE3":"#E8EDF1"
                                }

                                contentItem: Text {
                                    color:"#000000"
                                    horizontalAlignment:Text.AlignHCenter
                                    verticalAlignment : Text.AlignVCenter
                                    elide:Text.ElideRight
                                    text: parent.text
                                    font: parent.font
                                }
                            }
                            Switch {
                                id: switcherShare
                                spacing:facade.toPx(15)
                                height: facade.toPx(80)
                                anchors.verticalCenter: parent.verticalCenter;

                                Component.onCompleted: checked = loader.shared

                                font {
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }

                                onClicked:loader.shared=checked

                                text: checked==true?"Поделиться":"Не делиться"

                                indicator: Rectangle {
                                    radius:facade.toPx(25)
                                    x: parent.leftPadding;
                                    y: parent.height/2 - height/2;
                                    implicitWidth: facade.toPx(90)
                                    implicitHeight:facade.toPx(40)
                                    color: (parent.checked== true)?"#8610387F":"#8610387F"

                                    Rectangle {
                                        x:parent.parent.checked?
                                              parent.width-width-(parent.height-height)/2:
                                              (parent.height-height)/2;
                                        anchors.verticalCenter: parent.verticalCenter
                                        width: parent.parent.height/1.5
                                        height:parent.parent.height/1.5
                                        radius: width / 2;
                                        color: "#76CCCCCC"
                                    }
                                    Rectangle {
                                        radius: width / 2;
                                        x: parent.parent.checked?
                                              parent.width-width-(parent.height-height)/2:
                                              (parent.height-height)/2;
                                        anchors.verticalCenter: parent.verticalCenter
                                        width: parent.parent.height/1.8
                                        height:parent.parent.height/1.8
                                        color: parent.parent.down?
                                               "#12227C":
                                               (parent.parent.checked?"#10387E":"#3589E9")
                                    }
                                }

                                contentItem: Text {
                                    verticalAlignment:Text.AlignVCenter
                                    leftPadding: (parent.indicator.width + parent.spacing)
                                    opacity: enabled == true? 1.0: 0.3;
                                    text: parent.text
                                    font: parent.font
                                    color: "#000000"
                                }
                            }
                        }
                    }
                }
                Column {
                    width: parent.width
                    Button {
                        anchors {
                            left: parent.left
                            leftMargin: facade.toPx(30)
                        }
                        Component.onCompleted: {
                            pageHeight += height
                        }
                        height: facade.toPx(100)
                        width: parent.width-facade.toPx(60)

                        background: Rectangle {
                            clip: true
                            color: "#B8E3FE"
                            radius: facade.toPx(25);

                            Text {
                                color: "#000000"
                                font {
                                    pixelSize: facade.doPx(24);
                                    family:trebuchetMsNorm.name
                                }
                                anchors {
                                    left: parent.left
                                    leftMargin: facade.toPx(40)
                                    verticalCenter: parent.verticalCenter;
                                }
                                text: "Действие"
                            }

                            Image {
                                id: screenButtonImage
                                anchors {
                                    right: parent.right
                                    rightMargin: facade.toPx(40)
                                    verticalCenter: parent.verticalCenter
                                }
                                source:"ui/headers/icons/showMore.png"
                                height:facade.toPx(sourceSize.height*1.5)
                                width: facade.toPx(sourceSize.width *1.5)
                            }

                            RotationAnimator {
                                id: rotate
                                from: rolingup==true? 180:0
                                to: rolingup==true? 0:180
                                target: screenButtonImage
                                duration: 100
                            }
                        }
                        onClicked: {
                            rotate.start()
                            scrolDown.start()
                            rolingup =! rolingup;
                            if(!rolingup)
                                pageHeight -= facade.toPx(150)
                            else
                                pageHeight += facade.toPx(150)
                        }
                    }
                    Rectangle {
                        clip: true
                        color: "#B6D8EE"
                        visible: rolingup
                        anchors {
                            left: parent.left
                            leftMargin: facade.toPx(30)
                        }
                        radius: facade.toPx(25);
                        height: facade.toPx(150)
                        width: parent.width-facade.toPx(60)

                        Flickable {
                            width: parent.width;
                            height: {parent.height - facade.doPx(18);}

                            anchors {
                                left:parent.left
                                leftMargin: facade.toPx(30)
                                verticalCenter: parent.verticalCenter;
                            }
                            flickableDirection:Flickable.VerticalFlick
                            TextArea.flickable: TextArea {
                            readOnly: true;
                            color:"#000000"
                            font {
                                pixelSize: facade.doPx(18);
                                family:trebuchetMsNorm.name
                            }
                            wrapMode: Text.Wrap
                            text: target4
                            }
                        }
                    }
                }
                anchors.fill: parent
            }
        }
    }
}
