import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import "UiStyle" as UiStyle
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4

Item {
    id: rootPartner

    property int pageHeight: 0
    property int someHeight: 0
    property int plusHeight: 0

    Component.onCompleted: {
        partnerHeader.text = "Партнерам"
    }

    ListView {
        id: listView

        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        delegate: Rectangle {
            width: parent.width
            height: pageHeight + someHeight;
            Column {
                Rectangle {
                    id: logo
                    color: "#4396E9"
                    width: parent.width
                    height: facade.toPx(350)
                    Component.onCompleted: {
                        someHeight += height
                    }
                    Image {
                        anchors.centerIn: parent
                        width: facade.toPx(sourceSize.width *2)
                        height:facade.toPx(sourceSize.height*2)
                        source: "qrc:/ui/headers/handShake.png"
                    }
                }

                TabView {
                    id: tabview
                    width: parent.width
                    height: !loader.webview?
                                pageHeight-someHeight:
                                parent.height-logo.height;
                    onCurrentIndexChanged: {
                        if(index == 0) {
                            loader.webview = false;
                            listView.contentY=someHeight -
                                    partnerHeader.height +
                                    facade.toPx(40)
                        }
                    }

                    Tab {
                        title:qsTr("ОПИСАНИЕ")
                        component: Rectangle {
                            width: parent.width
                            height: tabview.currentIndex == 0?
                                        pageHeight =
                                        text1.implicitHeight +
                                        text2.implicitHeight +
                                        1* facade.toPx(40) +
                                        registration.height: 0
                            Loader {
                                anchors {
                                    fill: parent
                                    topMargin: 1
                                }
                                visible: loader.webview? true:false
                                source:event_handler.currentOSys()>0?"browser.qml":""
                            }
                            Column {
                                spacing: facade.toPx(40)
                                visible: loader.webview? 0: true
                                anchors {
                                    fill: parent
                                    leftMargin: 0.02 * parent.width
                                    rightMargin:0.02 * parent.width
                                }
                                Text {
                                    id: text1
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    font {
                                        pixelSize: facade.doPx(20);
                                        family:trebuchetMsNorm.name
                                    }
                                    text: "\nЕсли вы хотите разместить в нашем мобильном приложении - каталоге информацию о Вашей автомойке или сети автомоек, то Вам просто нужно зарегистрироваться у нас на сайте в качестве партнера и добавить информацию о Ваших автомойках в личном кабинете.\n\nВся информация о мойках, добавленных через наш основной веб-сайт или через менеджера компании будет отображаться:\n\n1. В данном приложении\n\n2. На основном сайте\n\n3. В соц. сетях (Vk, Fb, Twitter, G+)\n\nКроме того сотрудничая с нами, Вы сможете отправлять Push уведомления своим потенциальным и действующим клиентам у которых установлено мобильное приложение 'Все автомойки', проводить акции лояльности среди клиентов и устраивать приятные подарки для них."
                                }
                                Text {
                                    id: text2
                                    width: parent.width
                                    wrapMode: Text.Wrap
                                    horizontalAlignment: Text.AlignHCenter
                                    font {
                                        bold: true
                                        pixelSize: facade.doPx(20);
                                        family:trebuchetMsNorm.name
                                    }
                                    text: "С нашим приложением поток Ваших клиентов увеличится в разы!\n\nЗарегистрируйтесь у нас на сайте прямо сейчас, добавьте свою автомойку и уже через 20 минут, получайте первых клиентов!"
                                }
                                Button {
                                    id: registration
                                    width: facade.toPx(600)
                                    height:facade.toPx(110)
                                    anchors.horizontalCenter: parent.horizontalCenter

                                    text: "Зарегистрироваться";

                                    background: Rectangle {
                                        radius:facade.toPx(25);
                                        color:parent.down?"#1B5899":"#2882E6"
                                    }

                                    font {
                                    pixelSize: facade.doPx(22);
                                    family:trebuchetMsNorm.name
                                    }

                                    onClicked: {
                                        loader.urlLink = "http://www.vse-avtomoyki.ru/user/register"
                                        loader.webview = true
                                        listView.contentY = facade.toPx(350);
                                        // notificationClient.notification = "Developer is: VITTACH"
                                    }

                                    contentItem: Text {
                                        color:"#FFFFFF"
                                        horizontalAlignment:Text.AlignHCenter
                                        verticalAlignment : Text.AlignVCenter
                                        elide:Text.ElideRight
                                        text: parent.text
                                        font: parent.font
                                    }
                                }
                            }
                        }
                    }
                    Tab {
                        title: qsTr("КОММЕНТАРИИ")
                        component: Rectangle {
                            width: parent.width
                            height:tabview.currentIndex == 1?
                                        pageHeight = Math.max(
                                           plusHeight + innerRow.height,
                                           dialogSocials.pageHeight): 0;
                            Item {
                                id: innerRow
                                anchors {
                                    left: parent.left;
                                    right:parent.right
                                    leftMargin:facade.toPx(20)
                                }
                                height: Math.max(itext1.implicitHeight, ibutton1.height) + facade.toPx(40)

                                Text {
                                    id: itext1
                                    anchors {
                                        verticalCenter: parent.verticalCenter
                                    }

                                    font.bold: true
                                    font.family:trebuchetMsNorm.name
                                    font.pixelSize: facade.doPx(20);
                                    text: commetModel.count + " Комментарии";
                                }
                                Button {
                                    id: ibutton1
                                    background: Rectangle {
                                        color: "transparent"
                                    }
                                    anchors {
                                        right: parent.right
                                        rightMargin: facade.toPx(20)
                                        verticalCenter: parent.verticalCenter
                                    }
                                    onClicked: loader.social = true;

                                    font.family:trebuchetMsNorm.name
                                    font.pixelSize: facade.doPx(20);
                                    text: "ОПУБЛИКОВАТЬ"
                                }
                            }
                            ListView {
                                height:plusHeight
                                width: parent.width
                                anchors.top: innerRow.bottom
                                boundsBehavior: Flickable.StopAtBounds

                                Component.onCompleted: {
                                    commetModel.clear();
                                    for(var i = 0; i < loader.commentModel.length; i++) {
                                        commetModel.append({
                                            image: loader.commentModel[i].avatar,
                                            target1: loader.commentModel[i].comment,
                                            target0: loader.commentModel[i].fullName
                                        })
                                    }
                                }

                                model:ListModel {
                                    id: commetModel
                                    ListElement {
                                        image: ""; target0:""; target1:""
                                    }
                                }

                                delegate: Rectangle {
                                    x:facade.toPx(20)
                                    width: parent.width
                                    height: Math.max(bug.height, comment.implicitHeight) + facade.toPx(30)
                                    Component.onCompleted: {
                                        plusHeight += height
                                    }

                                    Rectangle {
                                        id: bug
                                        clip: true
                                        smooth: true
                                        visible: false
                                        width: facade.toPx(120)
                                        height:facade.toPx(120)
                                        anchors {
                                            top: parent.top
                                            topMargin: facade.toPx(20)
                                        }
                                        Image {
                                            source:image
                                            height:sourceSize.width>sourceSize.height? facade.toPx(120): sourceSize.height * (parent.width/sourceSize.width)
                                            width: sourceSize.width>sourceSize.height? sourceSize.width* (parent.height/sourceSize.height): facade.toPx(120)
                                        }
                                    }

                                    Image {
                                        id: mask
                                        smooth: true;
                                        visible:false
                                        source: "ui/uimask/roundMask.png"
                                        sourceSize: {
                                            Qt.size(facade.toPx(120), facade.toPx(120))
                                        }
                                    }

                                    OpacityMask {
                                        source: bug
                                        maskSource: mask
                                        anchors.fill: bug
                                    }

                                    Column {
                                        id: comment
                                        width: parent.width
                                        anchors {
                                            left: bug.right
                                            leftMargin: facade.toPx(40)
                                            verticalCenter: parent.verticalCenter
                                        }
                                        spacing:facade.toPx(20)
                                        Text {
                                            width: parent.width - bug.width - facade.toPx(80)
                                            wrapMode: Text.Wrap

                                            font.family:trebuchetMsNorm.name
                                            font.pixelSize: facade.doPx(20);
                                            font.bold: true
                                            text: target0
                                        }
                                        Text {
                                            width: parent.width - bug.width - facade.toPx(80)
                                            wrapMode: Text.Wrap

                                            font.family:trebuchetMsNorm.name
                                            font.pixelSize: facade.doPx(20);
                                            text: target1
                                        }
                                    }
                                }
                            }
                            UiStyle.DialogSocialm {
                                width: parent.width
                                height:dialogSocials.pageHeight
                                id: dialogSocials
                            }
                        }
                    }
                    style:TabViewStyle {
                        tab: Rectangle {
                            Component.onCompleted:someHeight+=implicitHeight
                            color: styleData.selected? "#5BB1FF": "#3383DF";
                            implicitHeight: facade.toPx(100)
                            implicitWidth:rootPartner.width/2+1
                            Text {
                                color: "#FFFFFF"
                                width: parent.width
                                wrapMode: Text.Wrap
                                anchors.centerIn: parent
                                horizontalAlignment:{
                                    Text.AlignHCenter
                                }
                                text: styleData.title
                                font {
                                    pixelSize: facade.doPx(20);
                                    family:trebuchetMsNorm.name
                                }
                            }
                            Rectangle {
                                anchors.bottom: {parent.bottom}
                                height: facade.toPx(5)
                                width: parent.width
                                color: "#5BB1FF"
                            }
                        }
                    }
                }
                anchors.fill: parent
            }
        }
        model: ListModel {ListElement {tr:""}}
        boundsBehavior: Flickable.StopAtBounds
    }
}
