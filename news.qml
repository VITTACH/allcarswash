import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import "UiStyle" as UiStyle
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: rootAbout
    color:"#90D3FE"

    property bool find: false

    property int pageHeight: 0
    property int someHeight: 0

    Component.onCompleted: {
        partnerHeader.text = "Новости"
    }

    ListView {
        id: listView

        width: parent.width
        anchors {
            top: parent.top
            bottom: parent.bottom
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        delegate: Rectangle {
            color: "transparent"
            width: parent.width
            height: pageHeight + someHeight;
            TabView {
                id: tabview
                width: parent.width
                height: pageHeight - someHeight
                onCurrentIndexChanged: {
                    if(index == 0)
                        listView.contentY=someHeight -
                                partnerHeader.height +
                                facade.toPx(40)
                }

                Tab {
                    title: qsTr("ВСЕ НОВОСТИ")
                    component: Rectangle {
                        color: "#90D3FE"
                        width: parent.width
                        height:tabview.currentIndex == 0?
                                   pageHeight = facade.toPx(500): 0;
                        //
                    }
                }
                Tab {
                    title: qsTr("TWITTER")
                    component: Rectangle {
                        color: "#90D3FE"
                        width: parent.width
                        height:tabview.currentIndex == 1?
                                   pageHeight = facade.toPx(500): 0;
                        //
                    }
                }
                style:TabViewStyle {
                    tab: Rectangle {
                        Component.onCompleted: {
                            someHeight += implicitHeight
                        }
                        color: styleData.selected?"#5BB1FF":"#3383DF"
                        implicitHeight: facade.toPx(100)
                        implicitWidth: rootAbout.width/2+1;
                        Text {
                            color: "#FFFFFF"
                            width: parent.width
                            wrapMode: Text.Wrap
                            anchors.centerIn: parent
                            horizontalAlignment: {Text.AlignHCenter;}

                            text: styleData.title;

                            font {
                                pixelSize: facade.doPx(20);
                                family:trebuchetMsNorm.name
                            }
                        }
                        Rectangle {
                            anchors.bottom: parent.bottom
                            height: facade.toPx(5)
                            width: parent.width
                            color: "#5BB1FF"
                        }
                    }
                }
            }
        }
        model: ListModel {ListElement {tr:""}}
        boundsBehavior: Flickable.StopAtBounds
    }
}
