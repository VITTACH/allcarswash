import QtQuick 2.7
import com.lasconic 1.0
import QtPositioning 5.6
import QtQuick.Controls 2.0
import "UiStyle" as UiStyle
import "js/xHRQuery.js" as XHRQuery
import "js/URLQuery.js" as URLQuery

ApplicationWindow {
    id: root
    width: 500
    height: 900
    visible: true
    title: qsTr("allcarswash")

    property bool flag: false;

    property bool find: false;

    Timer {
        id: backTimer
        interval: 500
        onTriggered: loader.back()
    }

    property variant locationOslo: QtPositioning.coordinate(60.0, 30.0);

    Component.onCompleted:if(event_handler.currentOSys()<1)getPosition()

    function strartPage() {
        privated.visitedPagesList.push(loader.source = "qrc:/basic.qml")
    }

    function listenBack(event){
        if(event.key == Qt.Key_Back || event.key == Qt.Key_Escape || event == true) {
            find = false;
            loader.focus = true
            event.accepted=true
            if(loader.dialog) {
                loader.dialog = !loader.dialog
            }
            else if(loader.social) {
                loader.social = !loader.social
                if (loader.source == "qrc:/qrscaner.qml") loader.back();
            }
            else if(loader.webview){
                loader.webview=!loader.webview
            }
            else
                backTimer.restart();
        }
    }

    function getWashs() {
    var request=new XMLHttpRequest()
    request.open('POST',"http://allcarswash.000webhostapp.com/post.php")
        request.onreadystatechange = function() {
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status===200) {
                    var result = "";
                    result = JSON.parse(request.responseText)
                    for (var i = 0; i < result.info.length; i = i + 1) {
                        var bufer=[]
                        var lat = result.info[i].lat;
                        var lon = result.info[i].lon;
                        for(var j=0;j<result.info[i].Picture.length;j++)
                        bufer.push(result.info[i].Picture[j])
                        loader.allWashModel.push({
                            url: "http://www.vk.com/vittach",
                            desc: result.info[i].description,
                            phone:"+" + result.info[i].Phone,
                            image:result.info[i].Type == 0?"ui/washs/car.png": (result.info[i].Type == 1?"ui/washs/bus.png": "ui/washs/you.png"),
                            coordinate:QtPositioning.coordinate(lat,lon),
                            target1: result.info[i].Street,
                                target3: result.info[i].price,
                                    target0: result.info[i].Name,
                                        time: result.info[i].Time,
                                            type: result.info[i].Type,
                                                picture: bufer,
                                                    target2: ""
                        })
                    }
                }
            }
        }
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        request.send("read=1")
    }

    function postToVk() {
        function callback(request) {
            if(request.status == 200) {
                var obj = JSON.parse(request.responseText)
                loader.commentModel.push({
                fullName: obj.response[0].last_name+ " "+ obj.response[0].first_name,
                avatar: obj.response[0].photo_100,
                comment: loader.fields[0],
                id: loader.userId
                })
            }
            else console.log('HTTP:',request.status, request.statusText)
        }

        var params = {
            owner_id: loader.userId,
            message:loader.fields[0]
        }
        XHRQuery.sendXHR('POST', "https://api.vk.com/method/wall.post?access_token=" + loader.aToken, callback, URLQuery.serializeParams(params))
        params = {
            user_ids: loader.userId,
            fields: 'photo_100',
            name_case: 'Nom'
        }
        XHRQuery.sendXHR('POST', "https://api.vk.com/method/users.get?access_token=" + loader.aToken, callback, URLQuery.serializeParams(params))
    }

    function setPosition() {
        for(var i = 0; i < loader.allWashModel.length; i=i+1)
            loader.allWashModel[i].target2 = (locationOslo.distanceTo(loader.allWashModel[i].coordinate)/1000).toFixed(1);
    }

    function getPosition() {
        var request = new XMLHttpRequest()
        request.open('GET', 'https://api.mylnikov.org/geolocation/wifi?v=1.1&bssid=A0:F3:C1:3B:6F:90');
        request.onreadystatechange=function(){
            if (request.readyState === XMLHttpRequest.DONE) {
                if (request.status && request.status === 200) {
                    console.log("response", request.responseText);
                    var result = JSON.parse(request.responseText);
                    var lat = result.data.lat;
                    var lon = result.data.lon;
                    locationOslo=QtPositioning.coordinate(lat,lon)
                    setPosition()
                }
            }
        }
        request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
        request.send()
    }

    PositionSource {
        active: true
        updateInterval: 1000
        onPositionChanged: {
            if(event_handler.currentOSys()>0)
            {
                var abscoordinate = position.coordinate
                locationOslo = QtPositioning.coordinate(abscoordinate.latitude,abscoordinate.longitude)
                if(flag) {
                    setPosition()
                    flag = false;
                }
            }
        }
    }

    QtObject {
        id: facade
        function toPx(dp) {
            return dp * (loader.dpi/160)
        }
        function doPx(dp) {
            return dp * (loader.dpi/160)*1.5;
        }
    }

    Loader {
        id: loader
        focus:true
        objectName: "loader"
        anchors.fill: parent

        property real dpi: 0

        property string aToken: ""
        property string userId: ""
        property var commentModel:[]

        property string urlLink: "";

        property bool shared: true
        property bool dialog: false;
        property bool social: false;
        property bool webview: false

        property int curCouponInd: 0
        property var couponsModel:[]

        property int curCityIndex: 0
        property var allCityModel:[]

        property int currentButon: 0
        property int curWashIndex:-1
        property var allWashModel:[]

        property int currentStock: 0

        Keys.onReleased: {listenBack(event);}

        QtObject {
            id: privated
            property var visitedPagesList: []
        }

        property variant fields:["","","",""]

        Component.onCompleted: {
            getWashs()
            flag=true;
            // todo: give this from server
            couponsModel.push({
                imgdisc: "ui/coupon/discount10.png",
                target0: "Скидка 10%",
                target1: "21 июня 2016г. - Актуальный",
                target2: 0,
                target3: 2,
                target4: "Lorem Ipsum чего-то там куда"
            })

            couponsModel.push({
                imgdisc: "ui/coupon/discount20.png",
                target0: "Скидка 20%",
                target1: "22 июня 2016г. - Актуальный",
                target2: 0,
                target3: 1,
                target4: "Lorem Ipsum, написанная рыба"
            })

            couponsModel.push({
                imgdisc: "ui/coupon/discount10.png",
                target0: "Скидка 10%",
                target1: "24 июня 2016г. - Актуальный",
                target2: 0,
                target3: 1,
                target4: "Lorem Ipsum чего-то там куда"
            })
            //-----------------------------------------

            // todo: give this from server
            allCityModel.push({
                target0: "Санкт-Петербург",
                target1: allWashModel.length.toString()
            })
            //-----------------------------------------

            strartPage()
        }

        function goTo(page) {
            privated.visitedPagesList.push(source=page)
        }
        function back() {
            if (privated.visitedPagesList.length > 0) {
                if(!(source == "qrc:/basic.qml")) {
                    privated.visitedPagesList.pop()
                    source = privated.visitedPagesList[privated.visitedPagesList.length - 1];
                }
            }
            else
                strartPage()
        }
    }

    UiStyle.HeaderWindow {
        visible: loader.source!="qrc:/basic.qml" && loader.source!="qrc:/qrscaner.qml"? 1: 0
        id: partnerHeader;
    }

    Item {
        visible: loader.source == "qrc:/coupon.qml"?1: 0
        anchors {
            top: parent.top
            right: parent.right
            rightMargin: facade.toPx(60)
            bottom: partnerHeader.bottom
        }

        Button {
            anchors.centerIn: parent
            width: shareButtonImage.width;
            height:shareButtonImage.height

            onClicked: {
                loader.focus = true
                shareUtils.share("Скачайте 'Все Автомойки' из Google Play","http://bit.ly/1U6qvMT")
            }

            Image {
                width: facade.toPx(sourceSize.width *1.5)
                height:facade.toPx(sourceSize.height*1.5)
                source: "ui/headers/icons/shareIcon.png";
                id:shareButtonImage
            }
            background: Rectangle {
                opacity: 0
            }
        }
    }

    Button {
        width: findImage.width;
        height:findImage.height

        visible: loader.source == "qrc:/coupons.qml" || loader.source == "qrc:/news.qml"? 1: 0
        onClicked: {
            find =! find
            inerText.text = "";
            if(find) inerText.focus=true
        }

        background: Image {
            id: findImage
            width: facade.toPx(sourceSize.width * 1.5);
            height: facade.toPx(sourceSize.height * 1.5);
            source: find==true? "ui/headers/icons/closeIcon.png": "ui/headers/icons/loopyIcon.png"
        }

        anchors {
            right: parent.right
            rightMargin: facade.toPx(40)
            verticalCenter: partnerHeader.verticalCenter;
        }
    }

    Rectangle {
        visible: loader.source == "qrc:/coupons.qml" || loader.source == "qrc:/news.qml"? find: 0
        id: filter
        color: "#225895"
        width: parent.width/2
        height: facade.toPx(80)
        radius: facade.toPx(25)

        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: partnerHeader.verticalCenter;
        }

        Row {
            spacing: facade.toPx(10)

            anchors {
                left: parent.left
                leftMargin: facade.toPx(20)
            }

            Image {
                id: inerImage
                width: facade.toPx(sourceSize.width);
                height:facade.toPx(sourceSize.height)
                anchors {
                    verticalCenter: parent.verticalCenter
                }
                source: "ui/headers/icons/loopyIcon.png";
            }
            TextField {
                id: inerText
                color: "#FFFFFF"
                height:facade.toPx(80)
                width: filter.width - inerImage.width - parent.spacing - facade.toPx(20);

                placeholderText: qsTr("Поиск");

                font {
                    pixelSize: facade.doPx(28);
                    family:trebuchetMsNorm.name
                }
                background:Rectangle{opacity:0}

                Keys.onReleased: {
                    listenBack(event)
                }
            }
        }
    }

    FontLoader {
        id: trebuchetMsItal
        source: "ui/fonts/TrebuchetMSI.ttf";
    }

    FontLoader {
        id:trebuchetMsLight
        source: "ui/fonts/SF_UI_displayL.otf"
    }

    FontLoader {
        id: trebuchetMsBold
        source: "ui/fonts/TrebuchetMSb.ttf";
    }

    FontLoader {
        id: trebuchetMsNorm
        source: "ui/fonts/TrebuchetMSn.ttf";
    }

    UiStyle.BusyIndicator{id: busyIndicator}

    UiStyle.DialogWindow {id: dialogAndroid}

    ShareUtils {id: shareUtils}
}
