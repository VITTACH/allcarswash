import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Controls 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4

Item {
    id: rootStocks

    property int pageWidth;

    Component.onCompleted: partnerHeader.text = "Все сообщения"

    function filterList() {
        for(var i = 0; i < washesModel.count; i++) {
            if (washesModel.get(i).type == currentStock|| currentStock == 0)
                washesModel.setProperty(i, "activity", 1)
            else
                washesModel.setProperty(i, "activity", 0)
        }
    }

    ListView {
        width: parent.width
        anchors {
            top: parent.top
            bottom: downRow.top
            topMargin: {partnerHeader.height - facade.toPx(10)}
        }

        Component.onCompleted: {
            washesModel.clear();
            busyIndicator.visible = true
            var arr= loader.allWashModel
            arr.forEach(function(item,i, arr) {
                if(currentButon == item.type)
                    washesModel.append({
                        activity: 1,
                            image:item.image,
                            type:Math.floor(Math.random()*3)+1,
                            target0: item.target0,
                                target1: item.target1,
                                    target2: item.target2,
                                        target3: item.target3
                    });
            });
            filterList()
            busyIndicator.visible =false
        }

        model:ListModel {
            id: washesModel
            ListElement {
                type: 0;
                image: "";
                target0: "";
                target1: "";
                activity: 0;
            }
        }
        delegate: Rectangle {
            visible: activity
            width: parent.width
            height: activity? Math.max(bug.height, inerColumn.implicitHeight) + facade.toPx(40): 0

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    parent.color="#90DDDDDD"
                }
                onReleased:{
                    parent.color = "#FFFFFF"
                }
                onPositionChanged: {
                    parent.color = "#FFFFFF"
                }
                onClicked: {
                    curWashIndex=index
                    loader.goTo("qrc:/wahsinfo.qml")
                }
            }

            Item {
                anchors.fill: parent
                Item {
                    anchors {
                        left: parent.left
                        leftMargin: facade.toPx(40);
                        verticalCenter: parent.verticalCenter
                    }
                    width: parent.width - anchors.leftMargin;
                    height: Math.max(bug.height, inerColumn.height)

                    Item {
                        id: item
                        width: bug.width
                        Rectangle {
                            id: bug
                            clip: true
                            smooth: true
                            visible: false
                            width: facade.toPx(200);
                            height:facade.toPx(200);
                            Image {
                                source: image
                                height:sourceSize.width > sourceSize.height? facade.toPx(200):sourceSize.height * (parent.width/sourceSize.width)
                                width: sourceSize.width > sourceSize.height? sourceSize.width*(parent.height/sourceSize.height): facade.toPx(200)
                            }
                        }

                        Image {
                            id: mask
                            smooth: true
                            visible: false
                            source: "ui/uimask/roundMask.png"
                            sourceSize: Qt.size(facade.toPx(300), facade.toPx(300))
                        }

                        OpacityMask {
                            source: bug
                            maskSource: mask
                            anchors.fill:bug
                        }
                    }

                    Column {
                        id: inerColumn
                        anchors {
                            left: item.right
                            leftMargin: facade.toPx(40);
                        }
                        width: parent.width - item.width - facade.toPx(60)
                        spacing:facade.toPx(20)
                        Text {
                            text: target0
                            color: "#000000"
                            width: parent.width
                            wrapMode: Text.Wrap
                            font {
                                family: trebuchetMsNorm.name;
                                pixelSize: {facade.doPx(24);}
                            }
                        }
                        Text {
                            text: target1
                            color: "#3589E9"
                            width: parent.width
                            wrapMode: Text.Wrap
                            font {
                                family: trebuchetMsNorm.name;
                                pixelSize: {facade.doPx(20);}
                            }
                        }
                    }
                }
                Image {
                    source: "ui/moreIcons/next.png"
                    width: facade.toPx(sourceSize.width *1.2)
                    height:facade.toPx(sourceSize.height*1.2)
                    anchors {
                        right: parent.right
                        rightMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    width: parent.width
                    color: "#DDDDDD"
                    height: 1
                }
            }
        }
    }

    Rectangle {
        id:downRow
        color:"#3383DF"
        width:parent.width
        height: facade.toPx(140);
        anchors.bottom: parent.bottom
        ListView {
            id: navButtons
            orientation:Qt.Horizontal
            anchors.horizontalCenter: parent.horizontalCenter

            height: parent.height
            width: (parent.width > facade.toPx(120)*4)? (4*pageWidth): (parent.width)

            model:  ListModel {
                    ListElement {
                    image: "ui/buttons/stocks/allButton.png";
                    target1: "Все";
                    }
                    ListElement {
                    image: "ui/buttons/stocks/linkButton.png"
                    target1: "Ссылки";
                    }
                    ListElement {
                    image:"ui/buttons/stocks/phoneButton.png";
                    target1:"Вкладки";
                    }
                    ListElement {
                    image:"ui/buttons/stocks/stocksButton.png"
                    target1: "Акции";
                    }
                }
            delegate: Button {
                onClicked: {
                    currentStock=index;
                    filterList();
                    switch(index)
                    {
                    case 0:
                        partnerHeader.text = "Все сообщения";
                        break;
                    case 1:
                        partnerHeader.text = "Ссылки"; break;
                    case 2:
                        partnerHeader.text = "Вкладки";break;
                    case 3:
                        partnerHeader.text = "Акции";break;
                    }
                }

                height: parent.height
                background: Rectangle {
                    color: parent.down? "#5BB1FF": (currentStock==index?"#5BB1FF":"#3383DF");
                }
                width: (pageWidth = (rootStocks.width< facade.toPx(950)?
                                    (rootStocks.width> facade.toPx(120)*4?
                                     rootStocks.width: facade.toPx(120)*4):
                                     facade.toPx(950))/4) - (index < 4? navButtons.spacing:0)

                Column {
                    Image {
                        anchors.horizontalCenter: {
                            parent.horizontalCenter
                        }
                        width: facade.toPx(sourceSize.width *1.5)
                        height:facade.toPx(sourceSize.height*1.5)
                        source:image;
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: 5
                    Text {
                        text: target1
                        color:"white"
                        horizontalAlignment: {Text.AlignHCenter;}
                        font {
                            family:trebuchetMsNorm.name
                            pixelSize: facade.doPx(16);
                        }
                        width: parent.parent.width
                        elide: Text.ElideRight
                    }
                }
            }
        }
    }
}
