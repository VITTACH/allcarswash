#ifndef COLORIMAGEPROVIDER_H
#define COLORIMAGEPROVIDER_H

#include <map>
#include <iostream>
#include <QEventLoop>
#include <QQuickImageProvider>
#include <QtNetwork/QNetworkReply>

using namespace std;

class ColorImageProvider : public QQuickImageProvider
{
    QImage image;
    map<string, QImage> myFirstMap;
protected:
    QNetworkAccessManager *manager;

public:
    ~ColorImageProvider();
    ColorImageProvider(ImageType type,Flags flags=0);
    QImage requestImage(const QString & id, QSize * size, const QSize & requestedSize);
};

#endif // COLORIMAGEPROVIDER_H
