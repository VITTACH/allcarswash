import QtQuick 2.7
import QtQuick.Controls 2.0

Rectangle {
    id: rootWashs

    Component.onCompleted: {
        partnerHeader.text = "Все мойки"
    }

    ListView {
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            topMargin: partnerHeader.height - facade.toPx(10)
        }

        Component.onCompleted: {
            citiesModel.clear()
            busyIndicator.visible = true
            var arr= loader.allCityModel
            arr.forEach(function(item,i,arr) {
                    citiesModel.append({
                        target0: item.target0,
                            target1: item.target1
                    });
            });
            busyIndicator.visible =false
        }

        model:ListModel {
            id: citiesModel
            ListElement {
                target0: "";
                target1: "";
            }
        }
        delegate: Rectangle {
            width: parent.width
            height: facade.toPx(120)

            MouseArea {
                anchors.fill: parent
                onPressed: {
                    parent.color="#90DDDDDD"
                }
                onReleased: {
                    parent.color = "#FFFFFF"
                }
                onPositionChanged: {
                    parent.color = "#FFFFFF"
                }
                onClicked: {
                    curCityIndex = index
                    loader.goTo("qrc:/typewash.qml")
                }
            }

            Item {
                anchors.fill: parent
                Item {
                    anchors {
                        left: parent.left
                        right: parent.right
                        leftMargin: facade.toPx(40)
                        verticalCenter: parent.verticalCenter
                    }
                    height: Math.max(text1.implicitHeight, text2.implicitHeight)
                    Text {
                        id: text1
                        text: target0
                        color: "#358BE6"
                        font {
                            family:trebuchetMsNorm.name
                            pixelSize: facade.doPx(24);
                        }
                    }
                    Text {
                        id: text2
                        text: target1
                        anchors {
                            right: parent.right
                            rightMargin:facade.toPx(50)
                        }

                        font {
                            bold: true
                            family:trebuchetMsNorm.name
                            pixelSize: facade.doPx(24);
                        }
                    }
                }
                Rectangle {
                    anchors.bottom: parent.bottom
                    width: parent.width
                    color: "#DDDDDD"
                    height: 1
                }
            }
        }
    }
}
