#include <QFile>
#include <QScreen>
#include <wrapper.h>
#include <QQmlContext>
#include <QQuickStyle>
#include <QApplication>
#include "httpnetwork.h"
#include "eventhandler.h"
#include "imageprocessor.h"
#include <QtWebView/QtWebView>
#include "notificationclient.h"
#include "colorimageprovider.h"
#include <QQmlApplicationEngine>

int currentSys = 0;

#ifdef Q_OS_IOS
currentSys = 2;
#endif

#ifdef Q_OS_ANDROID
#include "quickandroid.h"
#include "qadrawableprovider.h"
#include "qasystemdispatcher.h"
#include <QtAndroidExtras/QAndroidJniObject>
#include <QtAndroidExtras/QAndroidJniEnvironment>

JNIEXPORT jint JNI_OnLoad(JavaVM * javm, void*) {
    Q_UNUSED(javm);
    currentSys = 1;
    QASystemDispatcher::registerNatives();
    QAndroidJniObject::callStaticMethod <void>("org/qtproject/example/allcarswash/PushService",
                                               "start", "()V");
    return JNI_VERSION_1_6;
}
#endif

int main(int argc, char *argv []) {
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

#ifdef Q_OS_ANDROID
    QtWebView::initialize();
#endif

    httpnetwork httpNetwork;
    EventHandler eventhandler;
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:///");
    QQuickStyle::setStyle("UiStyle");
    QFile file(eventhandler.PathFile);
    eventhandler.currentSys= currentSys;
    if(file.open(QIODevice::ReadOnly)) {
        eventhandler.isPin = true;
        file.close();
    }

    qmlRegisterType<ImageProcessor>("ImageProcessor",1,0,"ImageProcessor");

    QQmlContext *context = engine.rootContext();
    Wrapper jwr;
    NotificationClient notifys;
    context->setContextProperty("caller", &jwr);
    context->setContextProperty("http_network", &httpNetwork);
    context->setContextProperty("event_handler", &eventhandler);
    context->setContextProperty("notificationClient", &notifys);

    engine.addImageProvider("imageprovider", new ColorImageProvider(QQmlImageProviderBase::Image));
    engine.load(QUrl("qrc:/loads.qml"));
    QObject *root_obj = engine.rootObjects().first();
    QObject *loader = root_obj->findChild <QObject*> ("loader");
    loader->setProperty
    ("dpi",QApplication::screens().at(0)->logicalDotsPerInch());

    return app.exec();
}
