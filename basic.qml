import QtQuick 2.7
import QtQuick.Controls   2.0
import QtGraphicalEffects 1.0

Item {
    id: rootBasic

    property int pageWidth

    property int pageHeight

    LinearGradient {
        anchors.fill:parent
        start:Qt.point(0, 0)
        end:  Qt.point(0, parent.height)
        gradient: Gradient {
            GradientStop {
                position: 0; color: "#4A9BE9";
            }
            GradientStop {
                position: 1; color: "#247FE5";
            }
        }
    }

    Rectangle {
        color: "transparent"
        width: parent.width
        height: parent.height - navButtons.height
        ListView {
            id: listView
            height: pageHeight
            width: parent.width
            boundsBehavior:Flickable.StopAtBounds
            anchors {
             verticalCenter:parent.verticalCenter
            }

            model:  ListModel {
                id: profileModel
                    ListElement {
                        image: "ui/logos/logo.png";
                        target1: "Помой машину без очереди!";
                        target2: "Выбери лучшую мойку!\nУзнай цены и запишись\nв одно касание";
                    }
                }
            delegate: Component {
                Rectangle {
                    color: "transparent"

                    width: parent.width
                    height: (pageHeight =
                            text1.implicitHeight +
                            text2.implicitHeight +
                            imageIcon.height +
                            3*inerColumn.spacing +
                            spacer1.height) +
                            (rootBasic.height<
                             text1.implicitHeight +
                             text2.implicitHeight +
                             imageIcon.height +
                             3*inerColumn.spacing +
                             navButtons.height +
                             facade.toPx(60)*2 +
                             spacer1.height?
                                 text1.implicitHeight +
                                 text2.implicitHeight +
                                 imageIcon.height +
                                 3*inerColumn.spacing +
                                 spacer1.height +
                                 facade.toPx(60)*2 +
                                 navButtons.height -
                                 rootBasic.height: 0)
                    Column {
                        id: inerColumn
                        width: parent.width
                        spacing: facade.toPx(20)

                        anchors {
                            topMargin: facade.toPx(60)
                            bottomMargin: facade.toPx(60)
                        }

                        anchors.verticalCenter: parent.verticalCenter
                        Image {
                            width: sourceSize.width / 1.4 *
                                   (parent.width<facade.toPx(1024)?
                                        parent.width: facade.toPx(1024))/500;
                            height:sourceSize.height/ 1.4 *
                                   (parent.width<facade.toPx(1024)?
                                        parent.width: facade.toPx(1024))/500;
                            anchors.horizontalCenter: parent.horizontalCenter
                            id: imageIcon
                            source: image
                        }
                        Rectangle {
                            id: spacer1
                            color: "transparent"
                            width: parent.width;
                            height: facade.toPx(60)
                        }
                        Text {
                            id: text1
                            text: target1
                            color: "#FFFFFF"
                            width: parent.width;
                            wrapMode: Text.Wrap;
                            horizontalAlignment: Text.AlignHCenter
                            font {
                                bold: true
                                family:trebuchetMsBold.name
                                pixelSize: facade.doPx(32);
                            }
                        }
                        Text {
                            id: text2
                            text: target2
                            color: "#FFFFFF"
                            width: parent.width;
                            wrapMode: Text.Wrap;
                            horizontalAlignment: Text.AlignHCenter
                            font {
                                family:trebuchetMsNorm.name
                                pixelSize: facade.doPx(24);
                            }
                        }
                    }
                }
            }
        }
    }

    Rectangle {
        width:parent.width
        height: pageWidth;
        anchors.bottom: parent.bottom
        ListView {
            spacing: 1
            id: navButtons
            orientation:Qt.Horizontal
            anchors.horizontalCenter: parent.horizontalCenter

            height: parent.height
            width: parent.width>facade.toPx(180)*4? 4*pageWidth: parent.width

            model:  ListModel {
                    ListElement {
                        image: "ui/buttons/basic/findButton.png";
                        target1: "Поиск";
                    }
                    ListElement {
                        image: "ui/buttons/basic/partButton.png";
                        target1: "Партнерам";
                    }
                    ListElement {
                        image: "ui/buttons/basic/shareButton.png"
                        target1: "Поделись";
                    }
                    ListElement {
                        image: "ui/buttons/basic/moreButton.png";
                        target1: "Еще";
                    }
                }
            delegate: Button {
                onClicked: {
                    switch(index) {
                    case 0: loader.goTo("qrc:/mapview.qml"); break;
                    case 1: loader.goTo("qrc:/partners.qml");break;
                    case 2: loader.goTo("qrc:/share.qml");break;
                    case 3: loader.goTo("qrc:/more.qml"); break;
                    }
                }

                height: parent.height
                background: Rectangle {
                    color: parent.down==true?"#D8D8D8":"#FFFFFF"
                }
                width: (pageWidth = (rootBasic.width < facade.toPx(950)?
                             (rootBasic.width > facade.toPx(180) * 4?
                                  rootBasic.width: facade.toPx(180) * 4):
                                  facade.toPx(950))/4) -
                                        (index<3? navButtons.spacing: 0);

                Column {
                    Image {
                        anchors.horizontalCenter: parent.horizontalCenter
                        width: facade.toPx(sourceSize.width *1.5)
                        height:facade.toPx(sourceSize.height*1.5)
                        source: image
                    }
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: facade.toPx(20);
                    Text {
                        elide:Text.ElideRight
                        horizontalAlignment: {Text.AlignHCenter;}
                        font.pixelSize: facade.doPx(20);
                        font.family:trebuchetMsNorm.name
                        width:parent.parent.width
                        color:"#247FE5"
                        text: {target1}
                    }
                }
            }
        }
    }
}
