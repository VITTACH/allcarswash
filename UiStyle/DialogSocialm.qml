import QtQuick 2.7
import QtQuick.Controls   2.0
import QtGraphicalEffects 1.0
import "../js/URLQuery.js" as URLQuery

Item {
    visible: loader.social;

    LinearGradient {
        anchors.fill:parent
        start:Qt.point(0,0)
        end:  Qt.point(0,parent.height)
        gradient:Gradient {
             GradientStop {
                position: 0; color: "#EE404040";
             }
             GradientStop {
                position: 1; color: "#70404040";
             }
        }
    }

    property int btHeight: facade.toPx(90);
    property int pageHeight: repeater.count * (btHeight + column.spacing) + text.implicitHeight + facade.toPx(80)

    Column {
        id: column
        width: parent.width
        spacing:facade.toPx(20)
        anchors.verticalCenter: parent.verticalCenter;
        Text {
            id: text
            color:"#FFFFFF"
            width: parent.width
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            font {
                family:trebuchetMsNorm.name
                pixelSize: facade.doPx(32);
            }
            anchors {
            horizontalCenter: parent.horizontalCenter;
            }
            text: "Выберите аккаунт для входа";
        }

        Repeater {
            id: repeater
            model: ["VK","FB", "Twitter", "Назад"]
            Button {
                text: modelData
                height:btHeight

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: facade.toPx(40)
                    rightMargin:facade.toPx(40)
                }

                font {
                    bold: true
                    pixelSize: facade.doPx(22);
                    family:trebuchetMsNorm.name
                }

                onClicked: {
                    listenBack(true)
                    switch(index) {
                    case 0:
                        partnerHeader.text= qsTr("Вконтакте")
                        var params = {
                            display: 'popup',
                            client_id: '5813771',
                            scope: 'wall,offline',
                            response_type:'token',
                            redirect_uri: 'http://oauth.vk.com/blank.html'
                        }

                        loader.urlLink = "https://oauth.vk.com/authorize?%1".arg(URLQuery.serializeParams(params))
                        loader.goTo("qrc:/webviews.qml")
                        break;
                    case 1:
                        partnerHeader.text= qsTr("Facebook")
                        var params = {
                            display: 'popup',
                            response_type: 'token',
                            scope:'publish_stream',
                            client_id: '396748683992616',
                            redirect_uri: 'https://www.facebook.com/connect/login_success.html'
                        }

                        loader.urlLink = "https://graph.facebook.com/oauth/authorize?%1".arg(URLQuery.serializeParams(params))
                        loader.goTo("qrc:/webviews.qml")
                        break;
                    case 2:
                        partnerHeader.text= qsTr("Instagram")
                        loader.urlLink = "http://www.instagram.com"
                        loader.goTo("qrc:/webviews.qml")
                        break;
                    }
                }

                background: Rectangle {
                    radius:facade.toPx(15)
                    color:parent.down?"#DADFE3":"#E8EDF1"
                }

                contentItem: Text {
                    color:"#000000"
                    horizontalAlignment:Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter;
                    elide: Text.ElideRight
                    text: parent.text
                    font: parent.font
                }
            }
        }
    }
}
