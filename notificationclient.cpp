#include "notificationclient.h"
#ifdef Q_OS_ANDROID
#include <QtAndroidExtras/QAndroidJniObject>
#endif

NotificationClient::NotificationClient(QObject *parent) : QObject(parent)
{
#ifdef Q_OS_ANDROID
    connect(this,SIGNAL(notificationChanged()),this,SLOT(updateAndroidNotification()));
#endif
}

void NotificationClient::updateAndroidNotification()
{
#ifdef Q_OS_ANDROID
    QAndroidJniObject javaNotification = QAndroidJniObject::fromString(m_notification);
    QAndroidJniObject::callStaticMethod<void>("org/qtproject/example/notification/NotificationClient", "notify", "(Ljava/lang/String;)V", javaNotification.object<jstring>());
#endif
}

void NotificationClient::setNotification(const QString &notification)
{
#ifdef Q_OS_ANDROID
    if (m_notification == notification) return;

    m_notification = notification;
    emit notificationChanged();
#endif
}

QString NotificationClient::notification() const
{
    return m_notification;
}
