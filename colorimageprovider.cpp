#include "colorimageprovider.h"

ColorImageProvider::ColorImageProvider(ImageType type, Flags flags):QQuickImageProvider(type,flags)
{manager = new QNetworkAccessManager;}

ColorImageProvider::~ColorImageProvider() {}

QImage ColorImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    string idx;
    idx = id.toStdString();
    if (myFirstMap.find(idx) == myFirstMap.end()) {
        QUrl url(id);
        QEventLoop eventLoop;
        QNetworkReply *reply;
        reply = manager->get(QNetworkRequest(url));
        QObject::connect(reply, SIGNAL(finished()), &eventLoop, SLOT(quit()));
        eventLoop.exec();
        if(reply->error()!=QNetworkReply::NoError){
            image.fill(qRgb(255,255,255));
            return image;
        }
        image = QImage::fromData(reply->readAll());
        size->setHeight(image.height());
        size->setWidth(image.width());
        myFirstMap.insert(pair<string,QImage>(idx,image));
    }
    return myFirstMap.at(idx);
}
