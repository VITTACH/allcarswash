import QtQuick 2.7
import QtQuick.Controls 2.0

Item {
    id: rootSocMedia
    property real pageWidth

    Component.onCompleted: {
        partnerHeader.text = qsTr("Мы в соц. сетях");
    }

    ListView {
        id: navButtons
        spacing:1
        anchors {
            top: parent.top
            horizontalCenter: parent.horizontalCenter
            topMargin: partnerHeader.height + facade.toPx(80)
        }
        height: pageWidth
        width: parent.width > facade.toPx(180)*4? 4*pageWidth: parent.width

        orientation:Qt.Horizontal
        boundsBehavior: Flickable.StopAtBounds

        model:  ListModel {
                ListElement {
                    image: "ui/buttons/fb.png";
                }
                ListElement {
                    image: "ui/buttons/in.png";
                }
                ListElement {
                    image: "ui/buttons/rss.png";
                }
                ListElement {
                    image: "ui/buttons/gp.png";
                }
            }
        delegate: Component {
            Rectangle {
                height: parent.height
                width: (pageWidth =
                        (rootSocMedia.width < facade.toPx(950)?
                             (rootSocMedia.width > facade.toPx(180)*4?
                                  rootSocMedia.width: facade.toPx(180)*4):
                                        facade.toPx(950))/4) - (index < 3?
                                             navButtons.spacing: 0)

                MouseArea {
                    anchors.fill: parent
                    onPressed: parent.color = "#D8D8D8"
                    onReleased:parent.color = "#FFFFFF"
                    onPositionChanged: parent.color = "#FFFFFF";
                    onClicked: {
                        switch(index) {
                        case 0:
                            partnerHeader.text= qsTr("Facebook")
                        loader.urlLink = "http://www.fb.com/groups/1574450369474202"
                            loader.goTo("qrc:/webviews.qml")
                            break;
                        case 1:
                            partnerHeader.text= qsTr("Instagram")
                            loader.urlLink = "http://www.instagram.com/VSEAVTOMOYKI"
                            loader.goTo("qrc:/webviews.qml")
                            break;
                        }
                    }
                }

                Image {
                    anchors.centerIn: parent
                    width: facade.toPx(sourceSize.width *1.5)
                    height:facade.toPx(sourceSize.height*1.5)
                    id: imageIcon
                    source: image
                }
            }
        }
    }
}
